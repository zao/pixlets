#include "dumb_source.h"
#include "dumb.h"
#include "vfs.h"

#include <gorilla/ga.h>
#include <gorilla/ga_internal.h>

#include <cmath>
#include <cstring>
#include <iostream>

namespace dumb
{
void init() {}
void terminate() {dumb_exit();}

struct sample_source_context
{
	std::shared_ptr<std::string> xm_data;
	DUMBFILE* dumbfile = nullptr;
	DUH* duh = nullptr;
	DUH_SIGRENDERER* sigrenderer = nullptr;
	bool exhausted = false;
};

struct sample_source
{
	ga_SampleSource sa;
	sample_source_context ctx;
};

namespace funcs
{
gc_int32 read(void* in_context, void* in_dst, gc_int32 in_numSamples,
                                   tOnSeekFunc /*in_onSeekFunc*/, void* /*in_seekContext*/)
{
	auto* source = (sample_source*)in_context;
	auto* ctx = &source->ctx;
	auto n = in_numSamples;
	auto fmt = source->sa.format;
	auto rendered = duh_render(ctx->sigrenderer, fmt.bitsPerSample, 0, 1.0,
		65536.0f/fmt.sampleRate, n, in_dst);
	if (rendered == 0)
		ctx->exhausted = true;
	return rendered;
}

gc_int32 end(void* in_context)
{
	auto* ctx = &((sample_source*)in_context)->ctx;
	return ctx->exhausted ? GC_TRUE : GC_FALSE;
}

gc_int32 ready(void* /*in_context*/, gc_int32 /*in_numSamples*/)
{
	return GC_TRUE;
}

//gc_int32 seek(void* in_context, gc_int32 in_sampleOffset);
//gc_int32 tell(void* in_context, gc_int32* out_totalSamples);

void close(void* in_context)
{
	auto* ctx = &((sample_source*)in_context)->ctx;
	std::cerr << "close\n";
	duh_end_sigrenderer(ctx->sigrenderer);
	unload_duh(ctx->duh);
	dumbfile_close(ctx->dumbfile);
	ctx->~sample_source_context();
}
}

ga_SampleSource* make_sample_source(std::string filename, bool loop)
{
	int const num_channels = 2;
	auto xm_data = vfs::slurp_file(filename);
	if (!xm_data) {
		std::cerr << "missing xm_data\n";
		abort();
		return nullptr;
	}
	auto* dumbfile = dumbfile_open_memory(xm_data->data(), xm_data->size());
	if (!dumbfile) {
		std::cerr << "failed dumbfile_open_memory\n";
		abort();
		return nullptr;
	}
	auto* duh = dumb_read_xm_quick(dumbfile);
	if (!duh) {
		std::cerr << "failed dumb_read_xm_quick\n";
		dumbfile_close(dumbfile);
		abort();
		return nullptr;
	}
	auto* sigrenderer = duh_start_sigrenderer(duh, 0, num_channels, 0);
	if (!sigrenderer) {
		std::cerr << "failed duh_start_sigrenderer\n";
		dumbfile_close(dumbfile);
		unload_duh(duh);
		abort();
		return nullptr;
	}
	auto* it_sigrenderer = duh_get_it_sigrenderer(sigrenderer);
	if (!loop) {
		dumb_it_set_loop_callback(it_sigrenderer, dumb_it_callback_terminate, nullptr);
	}
	auto* source = (sample_source*)gcX_ops->allocFunc(sizeof(sample_source));
	ga_sample_source_init(&source->sa);
	new(&source->ctx) sample_source_context();
	source->ctx.xm_data.reset(xm_data.release());
	source->ctx.dumbfile = dumbfile;
	source->ctx.duh = duh;
	source->ctx.sigrenderer = sigrenderer;
	auto& sa = source->sa;
	sa.readFunc = funcs::read;
	sa.endFunc = funcs::end;
	sa.readyFunc = funcs::ready;
	sa.closeFunc = funcs::close;
	sa.format.numChannels = num_channels;
	sa.format.bitsPerSample = 16;
	sa.format.sampleRate = 44100;
	return &sa;
}

std::shared_ptr<ga_Handle> load_xm(vfs::FilePath filepath, bool loop)
{
	if (!has_audio())
		return {};
	auto& audio = get_audio();
	auto source = make_sample_source(filepath, loop);
	auto h = ga_handle_create(audio._mixer, source);
	return std::shared_ptr<ga_Handle>(h, &ga_handle_destroy);
}
}