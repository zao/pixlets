#include "bmfont.h"
#include "vfs.h"
#include <cassert>
#include <cstring>
#include <iostream>

struct BinaryStream
{
	BinaryStream(std::string const& s)
	: _p(s.data())
	, _n(s.size())
	{}

	BinaryStream(char const* p, size_t n)
	: _p(p)
	, _n(n)
	{}

	bool empty() const {return _n == 0;}
	bool can_read(size_t n) const {return _n >= n;}
	size_t left() const {return _n;}
	void consume(size_t n) {assert(can_read(n)); _p += n; _n -= n;}

	void read_n(void* q, size_t n) {std::memcpy(q, _p, n); consume(n);}
	template <typename T>
	T& read(T& out) {read_n(&out, sizeof(T)); return out;}

	size_t read_ntbs(std::string& out)
	{
		if (!*_p) {
			out = "";
			return 0;
		}
		size_t i = 0;
		while (_p[i++])
			;
		std::vector<char> v(i);
		read_n(v.data(), i);
		out = v.data();
		return i-1;
	}

	uint8_t  read_u8()  {uint8_t  v; return read(v);}
	int8_t   read_s8()  {int8_t   v; return read(v);}
	uint16_t read_u16() {uint16_t v; return read(v);}
	int16_t  read_s16() {int16_t  v; return read(v);}
	uint32_t read_u32() {uint32_t v; return read(v);}
	int32_t  read_s32() {int32_t  v; return read(v);}

private:
	char const* _p;
	size_t _n;
};

namespace bmf
{
namespace bmfont
{
BMFont load(std::string filepath)
{
	BMFont f;
	auto data = vfs::slurp_file(filepath);
	auto base_dir = vfs::truncate_leaf(filepath);
	BinaryStream bs(*data);
	uint8_t sig[4];
	bs.read_n(sig, 4);
	if (sig[0] == 'i') {
		std::cerr << "unsupported text-based bmfont: " << filepath << std::endl;
		abort();
	}
	if (sig[0] != 'B' || sig[1] != 'M' || sig[2] != 'F' || sig[3] != 3) {
		std::cerr << "invalid bmfont: " << filepath << "\n";
		abort();
	}
	while (!bs.empty()) {
		uint8_t chunk_tag   = bs.read_u8();
		uint32_t chunk_size = bs.read_u32();
		switch (chunk_tag) {
			case 1: {
				Info& info = f.info;
				bs.read(info.font_size);
				uint8_t flags = bs.read_u8();
				info.is_smooth       = (flags & 0x1);
				info.is_unicode      = (flags & 0x2);
				info.is_italic       = (flags & 0x4);
				info.is_bold         = (flags & 0x8);
				info.is_fixed_height = (flags & 0x10);
				bs.read(info.charset);
				bs.read(info.stretch_height);
				bs.read(info.antialiasing);
				bs.read(info.padding_up);
				bs.read(info.padding_right);
				bs.read(info.padding_down);
				bs.read(info.padding_left);
				bs.read(info.spacing_horizontal);
				bs.read(info.spacing_vertical);
				bs.read(info.outline);
				bs.read_ntbs(info.font_name);
				break;
			}
			case 2: {
				Common& common = f.common;
				bs.read(common.line_height);
				bs.read(common.base);
				bs.read(common.scale_width);
				bs.read(common.scale_height);
				bs.read(common.pages);
				uint8_t flags = bs.read_u8();
				common.is_packed = (flags & 0x80);
				bs.read(common.alpha_channel);
				bs.read(common.red_channel);
				bs.read(common.green_channel);
				bs.read(common.blue_channel);
				break;
			}
			case 3: {
				Pages& pages = f.pages;
				do {
					std::string page_name;
					bs.read_ntbs(page_name);
					pages.page_names.push_back(base_dir + page_name);
					chunk_size -= page_name.size()+1;
				} while (chunk_size);
				break;
			}
			case 4: {
				Chars& chars = f.chars;
				size_t num_chars = chunk_size / 20;
				for (size_t i = 0; i < num_chars; ++i) {
					Chars::Char ch;
					bs.read(ch.id);
					bs.read(ch.x);
					bs.read(ch.y);
					bs.read(ch.width);
					bs.read(ch.height);
					bs.read(ch.x_offset);
					bs.read(ch.y_offset);
					bs.read(ch.x_advance);
					bs.read(ch.page);
					bs.read(ch.channel);
					chars.chars.push_back(ch);
				}
				break;
			}
			case 5: {
				KerningPairs& pairs = f.kerning_pairs;
				size_t num_pairs = chunk_size / 10;
				for (size_t i = 0; i < num_pairs; ++i) {
					KerningPairs::KerningPair pair;
					bs.read(pair.first);
					bs.read(pair.second);
					bs.read(pair.amount);
					pairs.pairs.push_back(pair);
				}
				break;
			}
			default: {
				std::cerr << "Unknown chunk " << +chunk_tag
					<< ", size " << chunk_size << std::endl;
				abort();
				bs.consume(chunk_size);
			}
		}
	}
	return f;
}
}
}