#pragma once
#include <glm/glm.hpp>
#include <glm/ext.hpp>

struct BoundingBox
{
	glm::ivec2 min, max;
};

BoundingBox make_bounding_box(glm::ivec2 extents, glm::ivec2 origin = glm::ivec2{0, 0})
{
	return { origin, extents - glm::ivec2(1, 1) - origin };
}

bool intersects(BoundingBox a, BoundingBox b)
{
	if (a.max.x < b.min.x || a.min.x > b.max.x) return false;
	if (a.max.y < b.min.y || a.min.y > b.max.y) return false;
	return true;
}