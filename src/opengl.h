#pragma once

#define GL_GLEXT_PROTOTYPES 1
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include <boost/noncopyable.hpp>

#include "string_token.h"

namespace gl
{
inline char const* stringify_enum(GLenum e)
{
	static std::map<GLenum, char const*> names = {
		{ GL_FLOAT,             "GL_FLOAT" },
		{ GL_FLOAT_VEC2,        "GL_FLOAT_VEC2" },
		{ GL_FLOAT_VEC3,        "GL_FLOAT_VEC3" },
		{ GL_FLOAT_VEC4,        "GL_FLOAT_VEC4" },
		{ GL_FLOAT_MAT2,        "GL_FLOAT_MAT2" },
		{ GL_FLOAT_MAT3,        "GL_FLOAT_MAT3" },
		{ GL_FLOAT_MAT4,        "GL_FLOAT_MAT4" },
		{ GL_FLOAT_MAT2x3,      "GL_FLOAT_MAT2x3" },
		{ GL_FLOAT_MAT2x4,      "GL_FLOAT_MAT2x4" },
		{ GL_FLOAT_MAT3x2,      "GL_FLOAT_MAT3x2" },
		{ GL_FLOAT_MAT3x4,      "GL_FLOAT_MAT3x4" },
		{ GL_FLOAT_MAT4x2,      "GL_FLOAT_MAT4x2" },
		{ GL_FLOAT_MAT4x3,      "GL_FLOAT_MAT4x3" },
		{ GL_INT,               "GL_INT" },
		{ GL_INT_VEC2,          "GL_INT_VEC2" },
		{ GL_INT_VEC3,          "GL_INT_VEC3" },
		{ GL_INT_VEC4,          "GL_INT_VEC4" },
		{ GL_UNSIGNED_INT,      "GL_UNSIGNED_INT" },
		{ GL_UNSIGNED_INT_VEC2, "GL_UNSIGNED_INT_VEC2" },
		{ GL_UNSIGNED_INT_VEC3, "GL_UNSIGNED_INT_VEC3" },
		{ GL_UNSIGNED_INT_VEC4, "GL_UNSIGNED_INT_VEC4" },
		{ GL_DOUBLE,            "GL_DOUBLE" },
		{ GL_DOUBLE_VEC2,       "GL_DOUBLE_VEC2" },
		{ GL_DOUBLE_VEC3,       "GL_DOUBLE_VEC3" },
		{ GL_DOUBLE_VEC4,       "GL_DOUBLE_VEC4" },
		{ GL_DOUBLE_MAT2,       "GL_DOUBLE_MAT2" },
		{ GL_DOUBLE_MAT3,       "GL_DOUBLE_MAT3" },
		{ GL_DOUBLE_MAT4,       "GL_DOUBLE_MAT4" },
		{ GL_DOUBLE_MAT2x3,     "GL_DOUBLE_MAT2x3" },
		{ GL_DOUBLE_MAT2x4,     "GL_DOUBLE_MAT2x4" },
		{ GL_DOUBLE_MAT3x2,     "GL_DOUBLE_MAT3x2" },
		{ GL_DOUBLE_MAT3x4,     "GL_DOUBLE_MAT3x4" },
		{ GL_DOUBLE_MAT4x2,     "GL_DOUBLE_MAT4x2" },
		{ GL_DOUBLE_MAT4x3,     "GL_DOUBLE_MAT4x3" },
		{ GL_ACTIVE_ATTRIBUTES, "GL_ACTIVE_ATTRIBUTES" },
		{ GL_ACTIVE_UNIFORMS,   "GL_ACTIVE_UNIFORMS" },
	};
	auto I = names.find(e);
	if (I == names.end())
		return "UNKNOWN";
	return I->second;
}
namespace tags
{
	struct Buffer;
	struct FragmentShader;
	struct Program;
	struct Texture1D;
	struct Texture2D;
	struct VertexShader;
}

template <typename Kind>
struct HandleTraits
{
	static GLuint create();
	static void destroy(GLuint);
};

#define UTIL_GL_DEFINE_HANDLE_TRAITS_SINGLE(Kind, Create, Destroy) template <> \
	struct HandleTraits<Kind> \
	{ \
		static GLuint create() {return Create();} \
		static void destroy(GLuint id) {Destroy(id);} \
	};

#define UTIL_GL_DEFINE_HANDLE_TRAITS_SINGLE_PARAM(Kind, Create, Param, Destroy) template <> \
	struct HandleTraits<Kind> \
	{ \
		static GLuint create() {return Create(Param);} \
		static void destroy(GLuint id) {Destroy(id);} \
	};

#define UTIL_GL_DEFINE_HANDLE_TRAITS_SEQUENCE(Kind, Create, Destroy) template <> \
	struct HandleTraits<Kind> \
	{ \
		static GLuint create() {GLuint id; Create(1, &id); return id;} \
		static void destroy(GLuint id) {Destroy(1, &id);} \
	};

UTIL_GL_DEFINE_HANDLE_TRAITS_SEQUENCE(tags::Buffer, glGenBuffers, glDeleteBuffers)
UTIL_GL_DEFINE_HANDLE_TRAITS_SINGLE_PARAM(tags::FragmentShader, glCreateShader, GL_FRAGMENT_SHADER, glDeleteShader)
UTIL_GL_DEFINE_HANDLE_TRAITS_SINGLE(tags::Program, glCreateProgram, glDeleteProgram)
UTIL_GL_DEFINE_HANDLE_TRAITS_SEQUENCE(tags::Texture1D, glGenTextures, glDeleteTextures)
UTIL_GL_DEFINE_HANDLE_TRAITS_SEQUENCE(tags::Texture2D, glGenTextures, glDeleteTextures)
UTIL_GL_DEFINE_HANDLE_TRAITS_SINGLE_PARAM(tags::VertexShader, glCreateShader, GL_VERTEX_SHADER, glDeleteShader)

template <typename Kind>
struct Handle : boost::noncopyable
{
	Handle() : _id(HandleTraits<Kind>::create()) {}
	explicit Handle(GLuint id) : _id(id) {}
	~Handle() {HandleTraits<Kind>::destroy(_id);}

	GLuint id() const {return _id;}

private:
	GLuint _id;
};

typedef Handle<tags::Buffer>         BufferHandle;
typedef Handle<tags::FragmentShader> FragmentShaderHandle;
typedef Handle<tags::Program>        ProgramHandle;
typedef Handle<tags::Texture1D>      Texture1DHandle;
typedef Handle<tags::Texture2D>      Texture2DHandle;
typedef Handle<tags::VertexShader>   VertexShaderHandle;

struct ScopedUseProgram : boost::noncopyable
{
	explicit ScopedUseProgram(ProgramHandle const& h) {glUseProgram(h.id());}
	~ScopedUseProgram() {glUseProgram(0);}
};

struct LocationMap : std::map<util::StringToken, GLuint>
{
	bool has(util::StringToken tok) const {return find(tok) != end();}
	bool has(std::string const& s) const {return has(util::string_token(s));}

	GLuint get(util::StringToken tok) const
	{
		auto I = find(tok);
		if (I != end())
			return I->second;
		return ~(GLuint)0;
	}

	GLuint get(std::string const& s) const
	{
		return get(util::string_token(s));
	}
};

struct MetaProgram
{
	ProgramHandle const& program() const {return _program;}
	LocationMap const& uniforms() const {return _uniforms;}
	LocationMap const& attribs() const {return _attribs;}

	ProgramHandle _program;
	LocationMap _uniforms;
	LocationMap _attribs;	
};

enum InstanceDivisor
{
	PER_VERTEX_DIVISOR = 0u,
	PER_INSTANCE_DIVISOR = 1u,
};

struct StreamBinding
{
	util::StringToken _name;
	GLenum _type;
	GLuint _count;
	size_t _vbo_index;
	GLboolean _normalized;
	GLuint _stride;
	void* _offset;
	GLuint _divisor;
};

struct IndexedModel
{
	typedef std::map<util::StringToken, StreamBinding> BindingTable;

	BufferHandle const& elements() const {return _elements;}
	GLsizei element_count() const {return _element_count;}
	GLenum element_type() const {return _element_type;}
	GLenum primitive_type() const {return _primitive_type;}
	BufferHandle const& buffer(size_t idx) const {return *_buffers[idx];}
	size_t num_buffers() const {return _buffers.size();}
	BindingTable const& bindings() const {return _bindings;}

	BufferHandle _elements;
	GLsizei _element_count;
	GLenum _element_type;
	GLenum _primitive_type;
	std::vector<std::shared_ptr<BufferHandle>> _buffers;
	BindingTable _bindings;
};

inline bool compile_shader(GLuint id, std::string source, std::string& log)
{
	GLint kind;
	glGetShaderiv(id, GL_SHADER_TYPE, &kind);
	auto* p = source.c_str();
	glShaderSource(id, 1, &p, nullptr);
	glCompileShader(id);
	GLint compile_status, info_log_length;
	glGetShaderiv(id, GL_COMPILE_STATUS, &compile_status);
	glGetShaderiv(id, GL_INFO_LOG_LENGTH, &info_log_length);
	if (info_log_length > 2) {
		std::vector<char> info_log(info_log_length);
		GLsizei num_chars = 0u;
		glGetShaderInfoLog(id, info_log.size(), &num_chars, info_log.data());
		log = std::string(info_log.data(), num_chars);
	}
	else {
		log = "";
	}
	return compile_status == GL_TRUE;
}

template <typename... Ids>
inline bool link_program(GLuint program, std::string& log, Ids... ids)
{
	for (auto shader_id : { ids... }) {
		glAttachShader(program, shader_id);
	}
	glLinkProgram(program);
	GLint link_status, info_log_length;
	glGetProgramiv(program, GL_LINK_STATUS, &link_status);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
	if (info_log_length > 2) {
		std::vector<char> info_log(info_log_length);
		GLsizei num_chars = 0u;
		glGetProgramInfoLog(program, info_log.size(), &num_chars, info_log.data());
		log = std::string(info_log.data(), num_chars);
	}
	else {
		log = "";
	}
	return link_status == GL_TRUE;
}

struct FormatLog
{
	FormatLog(std::string message, std::string log)
	: _message(message), _log(log)
	{}

	inline std::ostream& operator () (std::ostream& os) const
	{
		std::istringstream is(_log);
		std::string line;
		os << _message << ":" << std::endl;
		while (std::getline(is, line)) {
			os << "  " << line << std::endl;
		}
		return os;
	}

	std::string _message, _log;
};

inline std::ostream& operator << (std::ostream& os, FormatLog const& fmt)
{
	return fmt(os);
}

namespace meta_program
{
inline std::unique_ptr<MetaProgram> make_from_shader_sources(std::string vs_source, std::string fs_source)
{
	std::unique_ptr<MetaProgram> ret(new MetaProgram);
	VertexShaderHandle vs;
	FragmentShaderHandle fs;
	std::string vs_log, fs_log;
	bool vs_success = compile_shader(vs.id(), vs_source, vs_log);
	bool fs_success = compile_shader(fs.id(), fs_source, fs_log);

	if (vs_log.size()) {
		std::cerr << FormatLog("Vertex shader log", vs_log);
	}
	if (fs_log.size()) {
		std::cerr << FormatLog("Fragment shader log", fs_log);
	}
	if (!vs_success || !fs_success) {
		fprintf(stderr, "VS compile success: %d, FS compile success: %d\n", vs_success, fs_success);
		return nullptr;
	}

	auto prog = ret->program().id();
	std::string link_log;
	bool link_success = link_program(prog, link_log, vs.id(), fs.id());
	if (link_log.size()) {
		std::cerr << FormatLog("Shader program log", link_log);
	}
	if (!link_success) {
		fprintf(stderr, "Program link success: %d\n", link_success);
		return nullptr;
	}

	auto enumerate_locations = [&](LocationMap& map, GLenum type,
		std::function<void (GLuint, GLuint, GLsizei, GLsizei*, GLint*, GLenum*, GLchar*)> get_info,
		std::function<GLint (GLuint, char const*)> get_location)
	{
		GLint num_locations = 0;
		glGetProgramiv(prog, type, &num_locations);
		fprintf(stderr, "%s (%d):\n", stringify_enum(type), num_locations);
		for (GLint i = 0; i < num_locations; ++i) {
			std::vector<char> name(2048, '\0');
			GLint size = 0;
			GLenum type = 0;
			get_info(prog, i, name.size(), nullptr, &size, &type, name.data());
			GLint loc = get_location(prog, name.data());
			map[util::string_token(name.data())] = loc;
			fprintf(stderr, "  %u|%u (%s): (%d, %s)\n", i, loc, name.data(), size, stringify_enum(type));
		}
	};
	enumerate_locations(ret->_uniforms, GL_ACTIVE_UNIFORMS, glGetActiveUniform, glGetUniformLocation);
	enumerate_locations(ret->_attribs, GL_ACTIVE_ATTRIBUTES, glGetActiveAttrib, glGetAttribLocation);
	return ret;
}

inline void set_uniform(MetaProgram const& meta_program, util::StringToken name, glm::mat4 const& value)
{
	if (meta_program.uniforms().has(name)) {
		glUniformMatrix4fv(meta_program.uniforms().get(name), 1, GL_FALSE, glm::value_ptr(value));
	}
}

inline void set_uniform(MetaProgram const& meta_program, util::StringToken name, glm::vec4 const& value)
{
	if (meta_program.uniforms().has(name)) {
		glUniform4fv(meta_program.uniforms().get(name), 1, glm::value_ptr(value));
	}
}
}

inline bool fill_vbo(GLuint id, GLenum usage, void const* data, size_t num_bytes)
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
	glBufferData(GL_ARRAY_BUFFER, num_bytes, data, usage);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return true;
}

template <typename C>
bool fill_vbo(GLuint id, GLenum usage, C const& seq)
{
	void const* data = seq.data();
	size_t num_bytes = sizeof(typename C::value_type) * seq.size();
	return fill_vbo(id, usage, data, num_bytes);
}

namespace stream_binding
{
inline StreamBinding make(
	util::StringToken name,
	GLenum type,
	GLuint count,
	size_t vbo_index,
	GLboolean normalized,
	GLuint stride,
	void* offset,
	GLuint divisor)
{
	return { name, type, count, vbo_index, normalized, stride, offset, divisor };
}

template <typename... Rest>
StreamBinding make(
	std::string name,
	Rest... rest)
{
	return make(util::string_token(name), rest...);
}
}

namespace convert
{
inline uint32_t rgba(float r, float g, float b, float a)
{
	uint8_t rub = glm::saturate(r) * 255.0;
	uint8_t gub = glm::saturate(g) * 255.0;
	uint8_t bub = glm::saturate(b) * 255.0;
	uint8_t aub = glm::saturate(a) * 255.0;
	return (rub | gub << 8 | bub << 16 | aub << 24);
}

inline uint32_t rgba(float r, float g, float b)
{
	return rgba(r, g, b, 1.0f);
}

inline uint32_t rgba(glm::vec4 c)
{
	return rgba(c.r, c.g, c.b, c.a);
}

inline uint32_t rgba(glm::vec3 c)
{
	return rgba(c.r, c.g, c.b, 1.0f);
}
}
}