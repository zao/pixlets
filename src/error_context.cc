#include "error_context.h"
#include <cassert>
#include <cstdint>
#include <cstdio>

template <typename T, size_t N>
struct FixedVector
{
	T _elements[N];
	size_t _num_elements;
	enum { MAX_ELEMENTS = N };

	size_t capacity() const {return MAX_ELEMENTS;}
	size_t size() const {return _num_elements;}

	void push_back(T const& t)
	{
		assert(_num_elements < N);
		_elements[_num_elements++] = t;
	}

	void pop_back()
	{
		assert(_num_elements > 0);
		_num_elements--;
	}

	T const& operator [] (size_t i) const {return _elements[i];}
	T&       operator [] (size_t i)       {return _elements[i];}
};

#if __GNUC__ && (__GNUC__ < 4 || (__GNUC__ == 4 && __GNUC_MINOR__ < 8))
#define PIX_THREAD_LOCAL __thread
#elif _MSC_VER && _MSC_VER < 1800
#define PIX_THREAD_LOCAL __declspec(thread)
#else
#define PIX_THREAD_LOCAL thread_local
#endif

PIX_THREAD_LOCAL FixedVector<char const*, 256> _error_context_name;
PIX_THREAD_LOCAL FixedVector<char const*, 256> _error_context_data;

namespace pix
{
ErrorContext::ErrorContext(char const* name, char const* data)
{
	_error_context_name.push_back(name);
	_error_context_data.push_back(data);
}
ErrorContext::~ErrorContext()
{
	_error_context_name.pop_back();
	_error_context_data.pop_back();
}

void print_error_context()
{
	for (size_t i = 0; i < _error_context_name.size(); ++i) {
		auto data = _error_context_data[i];
		if (data) {
			fprintf(stderr, "When %s: %s\n",
				_error_context_name[i], _error_context_data[i]);
		}
		else {
			fprintf(stderr, "When %s\n",
				_error_context_name[i]);
		}
	}
}
}
