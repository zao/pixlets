#pragma once
#include <cstdint>
#include <deque>

typedef uint16_t EventId;

struct EventHeader
{
	EventId id;
	uint16_t size;
};

struct EventQueue
{
	bool empty() const {return _storage.empty();}
	
	EventHeader peek() const
	{
		EventHeader hdr;
		std::copy(_storage.begin(), _storage.begin() + sizeof(EventHeader), (char*)&hdr);
		return hdr;
	}

	void pop()
	{
		EventHeader hdr = peek();
		remove_event(hdr);
	}

	template <typename T>
	EventHeader pop(T& out)
	{
		EventHeader hdr;
		auto I0 = _storage.begin(), I1 = I0 + sizeof(EventHeader);
		std::copy(I0, I1, (char*)&hdr);
		auto I2 = I1 + hdr.size;
		std::copy(I1, I2, (char*)&out);
		remove_event(hdr);
		return hdr;
	}

	template <typename T>
	void push(EventId id, T const& t)
	{
		EventHeader hdr = { id, sizeof(T) };
		auto* t_ptr = (char const*)&t;
		push(hdr, t_ptr);
	}

	template <typename CharIterator>
	void push(EventHeader hdr, CharIterator from, CharIterator to)
	{
		push_header(hdr);
		push_data(from, to);
	}

	void push(EventHeader hdr, char const* p)
	{
		push_header(hdr);
		push_data(p, p + hdr.size);
	}

private:
	void push_header(EventHeader const& hdr)
	{
		auto* hdr_ptr = (char const*)&hdr;
		_storage.insert(_storage.end(), hdr_ptr, hdr_ptr + sizeof(EventHeader));
	}

	template <typename CharIterator>
	void push_data(CharIterator from, CharIterator to)
	{
		_storage.insert(_storage.end(), from, to);
	}

	void remove_event(EventHeader const& hdr)
	{
		size_t n = sizeof(EventHeader) + hdr.size;
		_storage.erase(_storage.begin(), _storage.begin() + n);
	}

	std::deque<char> _storage;
};
