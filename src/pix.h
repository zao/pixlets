#pragma once

#include "opengl.h"
#include "error_context.h"

#include <cstdint>
#include <map>
#include <memory>
#include <tuple>
#include <vector>

namespace pix
{
typedef uint32_t MaterialToken;
typedef std::tuple<uint8_t, uint8_t, uint8_t> MaterialCode;

void register_material(uint8_t r, uint8_t g, uint8_t b, std::unique_ptr<gl::MetaProgram>&& program);
MaterialToken material_token(uint8_t r, uint8_t g, uint8_t b);
gl::MetaProgram const& resolve_material(MaterialToken tok);

typedef std::function<bool (int /*key*/, int /*scancode*/, int /*action*/, int /*mods*/)> KeyboardHandler;
void register_keyboard_handler(KeyboardHandler);

struct PixelRenderable
{
	glm::ivec2 position;
	glm::vec4 color;
};

struct ViewState
{
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;
};

struct Batcher
{
	Batcher();

	std::vector<std::vector<PixelRenderable>> material_buckets;
};

struct MegaSprite
{
	struct MegaPixel
	{
		glm::vec4 color;
		MaterialToken material = 0;
	};

	struct Rect
	{
		glm::ivec2 min;
		glm::ivec2 max;
	};

	MegaSprite(int width, int height)
	: _rows(height)
	, _cols(width)
	, _pixels(width*height)
	{}

	void draw(glm::ivec2 offset) const;
	void draw(glm::ivec2 offset, Rect subset) const;

	MegaPixel&       pixel(size_t x, size_t y)       {return _pixels[y*_cols + x];}
	MegaPixel const& pixel(size_t x, size_t y) const {return _pixels[y*_cols + x];}

	size_t width() const {return _cols;}
	size_t height() const {return _rows;}

	size_t _rows, _cols;
	std::vector<MegaPixel> _pixels;
};

namespace mega_sprite
{
void draw(MegaSprite const& sprite, glm::ivec2 shift);
void draw(MegaSprite const& sprite, glm::ivec2 shift, MegaSprite::Rect subset);
}

struct RGBAImage : boost::noncopyable
{
	~RGBAImage();

	unsigned char const* pixel(int x, int y) const {return _bitmap + 4*(x + y*_width);}
	glm::ivec2 dimensions() const {return glm::ivec2(_width, _height);}
	int components() const {return _comp;}

	unsigned char* _bitmap = nullptr;
	int _width = 0, _height = 0, _comp = 0;
};

namespace rgba_image
{
bool load(RGBAImage& out, std::string filepath);
}

namespace mega_sprite
{
std::unique_ptr<MegaSprite> load(std::string filepath_base, std::string extension, bool load_material = true);
std::unique_ptr<MegaSprite> load(std::string filepath);
}

namespace indexed_model
{
std::unique_ptr<gl::IndexedModel> make_pixel_model();
}

struct WindowParameters
{
	std::string title;
	int width;
	int height;
};

struct SimulationParameters
{
	double frame_duration = 1.0/60.0;
	int playfield_width = 1;
	int playfield_height = 1;
};

void init(WindowParameters const& win_params, SimulationParameters const& sim_params);
void terminate();

int screen_width();
int screen_height();
bool running();
void flag_stop_running();

typedef std::function<void (float /*dt*/, float /*t*/)> Ticker;
double sim_tick(Ticker ticker);

typedef std::function<void ()> Drawer;
void draw_tick(Drawer drawer);

inline int center_interval(int small, int big) {return (big - small) / 2;}

#if __GNUC__ < 4 || (__GNUC__ == 4 && __GNUC_MINOR__ < 8)
#define PIX_NORETURN __attribute__((noreturn))
#else
#define PIX_NORETURN [[ noreturn ]]
#endif
PIX_NORETURN void crash(char const* file, int line, char const* reason);
#define PIX_CRASH(Reason) ::pix::crash(__FILE__, __LINE__, (Reason))
}