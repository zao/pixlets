#pragma once

namespace pix
{
struct ErrorContext
{
	ErrorContext(char const* name, char const* data = nullptr);
	~ErrorContext();

private:
	ErrorContext(ErrorContext const&);
	ErrorContext& operator = (ErrorContext const&);
};

void print_error_context();
}