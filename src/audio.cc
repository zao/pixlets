#include "audio.h"
#include <string>

#if PIX_USE_GORILLA_AUDIO
Audio::Audio()
: _mgr(nullptr)
, _mixer(nullptr)
, _stream_mgr(nullptr)
{
	gc_initialize(0);
	_mgr = gau_manager_create();
	_mixer = gau_manager_mixer(_mgr);
	_stream_mgr = gau_manager_streamManager(_mgr);
}

Audio::~Audio()
{
	gau_manager_destroy(_mgr);
	gc_shutdown();
}

void Audio::update()
{
	gau_manager_update(_mgr);
}

Audio::SoundHandle Audio::load_sound(std::string const& path, std::string const& type)
{
	auto sound = gau_load_sound_file(("content" + path + "." + type).c_str(), type.c_str());
	if (sound) {
		return SoundHandle(sound, &ga_sound_release);
	}
	return SoundHandle();
}

void Audio::play_sound(Audio::Sound* sound)
{
	if (sound) {
		ga_Handle* pew_handle = gau_create_handle_sound(_mixer, sound, &gau_on_finish_destroy, 0, 0);
		ga_handle_play(pew_handle);
	}
}

void Audio::play_handle(Audio::Handle* handle)
{
	if (handle) {
		ga_handle_play(handle);
	}
}

#else
	Audio::Audio() {}
	Audio::~Audio() {}

	void Audio::update() {}
	Audio::SoundHandle Audio::load_sound(std::string const& /*path*/, std::string const& /*type*/)
	{
		return SoundHandle();
	}

	void Audio::play_sound(Audio::Sound* /*sound*/) {}
};
#endif

static Audio* g_audio;

void setup_audio()
{
	g_audio = new Audio;
}

void shutdown_audio()
{
	delete g_audio;
	g_audio = nullptr;
}

bool has_audio()
{
	return g_audio;
}

Audio& get_audio()
{
	return *g_audio;
}