#include "opengl.h"
#include "pix.h"
#include "vfs.h"

#include <array>
#include <iostream>
#include <vector>

enum WorldConstants
{
	BASELINE = 32,
};

struct Player
{
	Player()
	: desire_move_left(false)
	, desire_move_right(false)
	, jumping(false)
	, falling(false)
	, jump_ascent(0)
	, position(0, BASELINE)
	, extents(10, 5)
	{}

	bool desire_move_left, desire_move_right;

	bool jumping;
	bool falling;
	int jump_ascent;
	glm::ivec2 position; // lower left corner of sprite
	glm::ivec2 extents;
} g_player;

bool game_keyboard_handler(int key, int scancode, int action, int mods)
{
	switch (key) {
		case GLFW_KEY_SPACE: {
			if (action == GLFW_PRESS) {
				if (!g_player.jumping && !g_player.falling) {
					g_player.jumping = true;
					g_player.falling = false;
					g_player.jump_ascent = 0;
				}
			}
			if (action == GLFW_RELEASE) {
				g_player.jumping = false;
				g_player.falling = true;
				g_player.jump_ascent = 0;
			}
			return true;
		}
		case GLFW_KEY_A: {
			if (action == GLFW_PRESS) {
				g_player.desire_move_left = true;
			}
			if (action == GLFW_RELEASE) {
				g_player.desire_move_left = false;
			}
			return true;
		}
		case GLFW_KEY_D: {
			if (action == GLFW_PRESS) {
				g_player.desire_move_right = true;
			}
			if (action == GLFW_RELEASE) {
				g_player.desire_move_right = false;
			}
			return true;
		}
	}
	return false;
}

enum PlayerParameters
{
	MAX_JUMP_HEIGHT = 10,
	X_MOVEMENT_PER_FRAME = 1,
	GRAVITY_MOVEMENT_PER_FRAME = 1,
};


int main()
{
	pix::WindowParameters win_params;
	win_params.width = 1280;
	win_params.height = 720;
	win_params.title = "pixlets";

	pix::SimulationParameters sim_params;
	sim_params.playfield_width = 320;
	sim_params.playfield_height = 180;
	sim_params.frame_duration = 1.0/60.0;

	pix::init(win_params, sim_params);
	{
		pix::register_keyboard_handler(&game_keyboard_handler);
		pix::register_material(0, 0, 127, gl::make_program(
			*vfs::slurp_file("/megapixel-vs.glsl"),
			*vfs::slurp_file("/megapixel_inverse-fs.glsl")));
		pix::register_material(0, 0, 0, gl::make_program(
			*vfs::slurp_file("/megapixel-vs.glsl"),
			*vfs::slurp_file("/megapixel-fs.glsl")));
		pix::MegaSprite car_sprite = *pix::load_mega_sprite("/simple_car", "png");
		pix::MegaSprite grass_sprite = *pix::load_mega_sprite("/BybDvS9", "png", false);

		while (pix::running()) {
			double t = pix::sim_tick([&](float dt, float t){
				if (g_player.desire_move_left != g_player.desire_move_right) {
					g_player.position.x += (g_player.desire_move_left ? -1 : 1)*X_MOVEMENT_PER_FRAME;
				}
				if (g_player.falling) {
					if (g_player.position.y == BASELINE) {
						g_player.falling = false;
					}
					else {
						g_player.position.y -= 1;
					}
				}
				if (g_player.jumping) {
					if (g_player.jump_ascent <= MAX_JUMP_HEIGHT) {
						g_player.position.y += 1;
						g_player.jump_ascent += 1;
					}
					else {
						g_player.jumping = false;
						g_player.falling = true;
						g_player.jump_ascent = 0;
					}
				}
			});
			pix::draw_tick([&]{
				glm::ivec2 car_pos{50 + (int)(20*sin(t)), 30 + (int)(20*cos(t*1.7))};
				for (int i = 0; i < 10; ++i) {
					grass_sprite.draw(glm::ivec2{i*grass_sprite.width(), 0});
				}
				car_sprite.draw(car_pos);
				car_sprite.draw(g_player.position);
			});
		}
	}
	pix::terminate();
}