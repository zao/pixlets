#include "sprite_sheet.h"
#include "string_token.h"

namespace pix
{	
struct SpriteFileSpec
{
	std::string filepath;
	std::string ext = "png";
	bool omit_material = false;
};
}

namespace json
{
	inline glm::ivec2 get_ivec2(Json::Value v)
	{
		return glm::ivec2{
			v[0].asInt(),
			v[1].asInt()
		};
	}

	inline pix::SpriteFileSpec get_sprite_file_spec(Json::Value v)
	{
		pix::SpriteFileSpec ret;
		if (!v.isMember("filepath")) {
			std::cerr << "sprite file spec missing \"filepath\": " << v << std::endl;
			abort();
		}
		ret.filepath = v["filepath"].asString();
		if (v.isMember("ext"))
			ret.ext = v["ext"].asString();
		if (v.isMember("omit_material"))
			ret.omit_material = v["omit_material"].asBool();
		return ret;
	}
}

namespace pix
{
namespace sprite_sheet
{
	std::shared_ptr<SpriteSheet> load_from_json_value(Json::Value const& v)
	{
		pix::SpriteFileSpec spec = json::get_sprite_file_spec(v.get("sprite", Json::Value()));
		auto sprite = std::shared_ptr<MegaSprite>(mega_sprite::load(spec.filepath, spec.ext, !spec.omit_material));
		glm::ivec2 default_size;
		bool has_default_size = false;
		if (v.isMember("default_size")) {
			default_size = json::get_ivec2(v["default_size"]);
			has_default_size = true;
		}
		auto entries = v.get("entries", Json::Value());
		auto ret = std::make_shared<SpriteSheet>();
		for (auto key : entries.getMemberNames()) {
			auto name = util::string_token(key);
			auto& src_entry = entries[key];
			glm::ivec2 size;
			if (src_entry.isMember("size")) {
				size = json::get_ivec2(src_entry["size"]);
			}
			else {
				if (!has_default_size) {
					std::cerr << "sprite sheet lacks both size and default_size" << std::endl;
					abort();
				}
				size = default_size;
			}
			SpriteSheet::Entry entry = {
				name,
				json::get_ivec2(src_entry["pos"]),
				size,
				sprite
			};
			ret->entries.push_back(entry);
		}
		return ret;
	}

	std::shared_ptr<SpriteSheet> load_from_json_memory(char const* p, size_t n)
	{
		Json::Value v;
		Json::Reader r;
		if (!r.parse(p, p + n, v)) {
			std::cerr << "json from memory failed:\n";
			std::cerr << r.getFormattedErrorMessages() << std::endl;
			abort();
		}
		return load_from_json_value(v);
	}

	std::shared_ptr<SpriteSheet> load_from_json_file(vfs::FilePath filepath)
	{
		auto data = vfs::slurp_file(filepath + ".json");
		return load_from_json_memory(data->data(), data->size());
	}

	void draw(SpriteSheet::Entry const& entry, glm::ivec2 shift)
	{
		MegaSprite::Rect r = {
			entry.pos, entry.pos + entry.size
		};
		mega_sprite::draw(*entry.sprite, shift, r);
	}

	void draw(SpriteSheet const& sheet, util::StringToken name, glm::ivec2 shift)
	{
		for (auto& entry : sheet.entries) {
			if (entry.name == name) {
				draw(entry, shift);
				return;
			}
		}
		std::cerr << "sprite sheet entry missing: " << util::redeem_token(name) << std::endl;
	}
}
}