#include "bitmap_font.h"
#include "bmfont.h"
#include "vfs.h"

namespace pix
{
BitmapFont::Character const& BitmapFont::get(uint32_t ch) const
{
	uint32_t const INVALID_GLYPH_INDEX = 0xFFFFFFFF;
	auto chI = _characters.find(ch);
	if (chI == _characters.end())
		return _characters.find(INVALID_GLYPH_INDEX)->second;
	return chI->second;
}

int BitmapFont::kern(uint32_t a, uint32_t b) const
{
	auto aI = _kerning_infos.find(a);
	if (aI == _kerning_infos.end()) return 0;
	auto bI = aI->second.find(b);
	if (bI == aI->second.end()) return 0;
	return bI->second.amount;
}

namespace bitmap_font
{
std::shared_ptr<BitmapFont> make_from_bmfont(bmf::BMFont const& source)
{
	auto ret = std::make_shared<BitmapFont>();
	ret->baseline_shift = source.common.base;
	ret->line_height = source.common.line_height;
	for (auto page : source.pages.page_names) {
		auto page_sprite = pix::mega_sprite::load(page);
		auto p = std::shared_ptr<MegaSprite>(page_sprite.release());
		ret->_pages.push_back(p);
	}
	for (auto src : source.chars.chars) {
		BitmapFont::Character ch;
		ch.sprite = ret->_pages[src.page].get();
		glm::ivec2 min{src.x, src.y};
		ch.sheet_region.min = min;
		ch.sheet_region.max = min + glm::ivec2{src.width, src.height};
		ch.origin_offset = glm::ivec2{src.x_offset, src.y_offset};
		ch.advance = src.x_advance;
		ret->_characters[src.id] = ch;
	}
	for (auto src : source.kerning_pairs.pairs) {
		BitmapFont::KerningInfo kp;
		kp.amount = src.amount;
		ret->_kerning_infos[src.first][src.second] = kp;
	}
	return ret;
}

void draw_string(BitmapFont const& font, std::wstring s, glm::ivec2 shift)
{
	shift.y += font.baseline_shift;
	wchar_t previous = 0;
	for (auto ch : s) {
		auto& c = font.get(ch);
		auto kern = font.kern(previous, ch);
		shift.x += kern;
		auto cell_height = c.sheet_region.max.y - c.sheet_region.min.y;
		auto origin_offset = glm::ivec2{
			c.origin_offset.x,
			-cell_height - c.origin_offset.y};
		pix::mega_sprite::draw(*c.sprite, shift + origin_offset, c.sheet_region);

		shift.x += c.advance;
		previous = ch;
	}
}
}
}