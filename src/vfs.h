#pragma once
#include <memory>
#include <string>

namespace vfs
{
typedef std::string FilePath;

std::unique_ptr<std::string> slurp_file(FilePath filepath);
FilePath truncate_leaf(FilePath source);
std::string remove_extension(FilePath& source);
}