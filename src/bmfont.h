#pragma once
#include <cstdint>
#include <memory>
#include <string>
#include <vector>

namespace bmf
{
struct Info
{
	int16_t font_size;

	bool is_smooth;
	bool is_unicode;
	bool is_italic;
	bool is_bold;
	bool is_fixed_height;

	uint8_t charset;
	uint16_t stretch_height;
	uint8_t antialiasing;
	uint8_t padding_up;
	uint8_t padding_right;
	uint8_t padding_down;
	uint8_t padding_left;
	uint8_t spacing_horizontal;
	uint8_t spacing_vertical;

	uint8_t outline;
	std::string font_name;
};

struct Common
{
	uint16_t line_height;
	uint16_t base;
	uint16_t scale_width;
	uint16_t scale_height;
	uint16_t pages;
	bool is_packed;
	uint8_t alpha_channel;
	uint8_t red_channel;
	uint8_t green_channel;
	uint8_t blue_channel;
};

struct Pages
{
	std::vector<std::string> page_names;
};

struct Chars
{
	struct Char
	{
		uint32_t id;
		uint16_t x;
		uint16_t y;
		uint16_t width;
		uint16_t height;
		int16_t x_offset;
		int16_t y_offset;
		int16_t x_advance;
		uint8_t page;
		uint8_t channel;
	};
	std::vector<Char> chars;
};

struct KerningPairs
{
	struct KerningPair
	{
		uint32_t first;
		uint32_t second;
		int16_t amount;
	};
	std::vector<KerningPair> pairs;
};

struct BMFont
{
	Info info;
	Common common;
	Pages pages;
	Chars chars;
	KerningPairs kerning_pairs;
};

namespace bmfont
{
BMFont load(std::string filepath);
}
}