#pragma once
#include "geometry_math.h"

#include "audio.h"
#include "bmfont.h"
#include "bitmap_font.h"
#include "event_queue.h"
#include "game_screen.h"
#include "pix.h"

#include <algorithm>
#include <list>
#include <memory>

static pix::SimulationParameters const sim_params = []{
	pix::SimulationParameters sp;
	sp.playfield_width = 256;
	sp.playfield_height = 144;
	sp.frame_duration = 1.0/60.0;
	return sp;
}();

static double const TIME_BETWEEN_SHOTS = 0.3;
static double const SHOT_TIMING_TOLERANCE = 0.1;

static int const GAME_START_DURATION = 60;
static int const RESPAWN_BREATHER_DURATION = 120;

static BoundingBox const player_bullet_bounds{
	glm::ivec2{0, 0},
	glm::ivec2{1, 3}};

static BoundingBox const enemy_bullet_bounds{
	glm::ivec2{0, 0},
	glm::ivec2{0, 3}};

static glm::ivec2 const TURRET_OFFSET{7, 10};
static glm::ivec2 const PLAYER_SIZE{16, 10};
static glm::ivec2 const BULLET_SIZE{2, 4};
static glm::ivec2 const BARRIER_SIZE{20, 8};
static glm::ivec2 const ALIEN_SIZE{12, 8};
static glm::ivec2 const ALIEN_FORMATION{12, 5};
static glm::ivec2 const ALIEN_STRIDE{15, 10};
static glm::ivec2 const PLAYER_SPAWN_POSITION{pix::center_interval(PLAYER_SIZE.x, sim_params.playfield_width), 0};
static glm::ivec2 const ALIEN_SPAWN_OFFSET{
	pix::center_interval(ALIEN_FORMATION.x * ALIEN_STRIDE.x, sim_params.playfield_width),
	sim_params.playfield_height - ALIEN_FORMATION.y * ALIEN_STRIDE.y - 8};
static int const BARRIER_ALTITUDE = 12;
static int const NUM_BARRIERS = 5;
static int const STEPS_PER_ROW = 70;

enum InputEvents
{
	KEYBOARD_EVENT = 1,
};

struct KeyboardEvent
{
	static InputEvents const ID = KEYBOARD_EVENT;
	int key, scancode, action, mods;
};

struct WorldEntity
{
	glm::ivec2 position;
	BoundingBox bounds;
};

enum class Owner
{
	PLAYER,
	FOE,
};

struct Player : WorldEntity
{
};

struct Bullet : WorldEntity
{
	glm::ivec2 velocity;
	Owner owner;
};

struct Barrier : WorldEntity
{
	int health = 4;
};

struct Alien : WorldEntity
{
	enum Kind
	{
		Foo, Bar, Baz
	};
	Kind kind;
};

inline bool collides(WorldEntity a, WorldEntity b)
{
	b.bounds.min += b.position - a.position;
	b.bounds.max += b.position - a.position;
	return intersects(a.bounds, b.bounds);
}

template <typename I>
bool PointeeIdentityLess(I a, I b)
{
	return std::less<decltype(&*a)>()(&*a, &*b);
}

inline bool operator < (std::list<Bullet>::iterator a, std::list<Bullet>::iterator b) {return PointeeIdentityLess(a, b);}
inline bool operator < (std::list<Alien>::iterator a,  std::list<Alien>::iterator b)  {return PointeeIdentityLess(a, b);}

inline int alien_value(Alien::Kind kind)
{
	if (kind == Alien::Foo) return 10;
	if (kind == Alien::Bar) return 20;
	if (kind == Alien::Baz) return 30;
	return 0;
}

struct PlayerIntent
{
	bool want_move_left  = false;
	bool want_move_right = false;
	bool want_shoot = false;

	bool cheat_decimate = false;

	bool interpret_key(int key, int action);
};

struct GameArtAssets
{
	GameArtAssets();

	std::shared_ptr<pix::MegaSprite> win_message, lose_message,
		player_sprite, shot_sprite, barrier_sprite,
		foo_sprite, bar_sprite, baz_sprite;
	std::shared_ptr<pix::MegaSprite> alien_sprites[3];
};

struct GameSoundAssets
{
	GameSoundAssets();

	Audio::HandleHandle intro_music, bgm_music;
	Audio::SoundHandle pew_sound, death_sound,
		alien_die_sound, alien_pew_sound,
		barrier_hit_sound, barrier_death_sound;
};

struct CommonArtAssets
{
	CommonArtAssets();

	std::shared_ptr<pix::BitmapFont> font;
};

enum class GamePhase
{
	GAME_STARTING,
	LIFE_STARTING,
	GAME_RUNNING,
	PLAYER_DEATH,
	RESPAWN_BREATHER,
	GAME_WON,
	GAME_LOST,
};

struct GameState
{
	GameState()
	{
		player.bounds = make_bounding_box(PLAYER_SIZE);
	}

	int random_alien_shot_delay()
	{
		return 10 + (rand() % 100);
	}

	void reset_game()
	{
		frames_in_current_phase = 0;
		game_phase = GamePhase::GAME_STARTING;
		lives = 3;
		score = 0;
	}

	void reset_board()
	{
		bullets.clear();
		barriers.clear();
		aliens.clear();

		for (double col = 0.0; col < (double)ALIEN_FORMATION.x; ++col) {
			for (double row = 0.0; row < (double)ALIEN_FORMATION.y; ++row) {
				Alien a = {};
				if (row < 2)      { a.kind = Alien::Foo; }
				else if (row < 4) { a.kind = Alien::Bar; }
				else              { a.kind = Alien::Baz; }
				a.position = ALIEN_SPAWN_OFFSET + glm::ivec2{col, row} * ALIEN_STRIDE;
				a.bounds = make_bounding_box(ALIEN_SIZE);
				aliens.push_back(a);
			}
		}

		for (double i = 0.0; i < (double)NUM_BARRIERS; ++i) {
			Barrier b = {};
			int x = sim_params.playfield_width*(i+0.5) / (NUM_BARRIERS);
			x -= BARRIER_SIZE.x / 2;
			b.position = glm::ivec2{x, BARRIER_ALTITUDE};
			b.bounds = make_bounding_box(BARRIER_SIZE);
			barriers.push_back(b);
		}

		last_shot_time = -FLT_MAX;
		frames_to_next_alien_shot = random_alien_shot_delay();
		player_visible = false;
	}

	void reset_player()
	{
		player.position = PLAYER_SPAWN_POSITION;
		shot_intent_time = 0.0;
		shot_requested = false;
	}

	Bullet make_player_bullet(glm::ivec2 source)
	{
		Bullet b;
		b.owner = Owner::PLAYER;
		(WorldEntity&)b = WorldEntity{source, player_bullet_bounds};
		b.velocity = glm::ivec2{0, 2};
		return b;
	}

	Bullet make_enemy_bullet(glm::ivec2 source)
	{
		Bullet b;
		b.owner = Owner::FOE;
		(WorldEntity&)b = WorldEntity{source-BULLET_SIZE.y, enemy_bullet_bounds};
		b.velocity = glm::ivec2{0, -1};
		return b;
	}

	bool player_bullet_exists()
	{
		return std::any_of(bullets.begin(), bullets.end(), [](Bullet b){
			return b.owner == Owner::PLAYER;
		});
	}

	size_t enemy_bullet_count()
	{
		return std::count_if(bullets.begin(), bullets.end(), [](Bullet b){
			return b.owner == Owner::FOE;
		});
	}

	void change_phase(GamePhase new_phase)
	{
		game_phase = new_phase;
		frames_in_current_phase = 0;
	}

	void add_score(size_t amount)
	{
		score += amount;
	}

public:
	// game state
	int frames_in_current_phase;
	GamePhase game_phase;
	int lives;
	int score;

	// board state
	std::list<Bullet> bullets;
	std::list<Barrier> barriers;
	std::list<Alien> aliens;

	int alien_idle_counter = 0;
	int alien_row = 0;
	int alien_steps_taken = STEPS_PER_ROW/2;

	double last_shot_time;
	bool player_visible;

	double shot_intent_time;
	bool shot_requested;
	int frames_to_next_alien_shot;

	// player state
	Player player;
	PlayerIntent intent;
};

enum class Sound
{
	START,
	PLAYER_PEW,
	PLAYER_DEATH,
	FOE_PEW,
	FOE_DEATH,
	BARRIER_HIT,
	BARRIER_DEATH,
	GAME_WON,
	GAME_LOST,
};

struct PlayScreen : GameScreen
{
	PlayScreen(EventQueue* input_queue, CommonArtAssets* common_art, GameArtAssets* game_art, pix::SimulationParameters sim_params);

	void sim_tick(float dt, float t);
	void vis_tick();

private:
	void play_bite(Sound key);

private:
	EventQueue* _input_queue;
	GameArtAssets* _art;
	GameSoundAssets _snd;
	CommonArtAssets* _common_art;
	pix::SimulationParameters _sim_params;

	GameState _game;
	std::shared_ptr<pix::MegaSprite> message_sprite;

	std::wstring _lives_message;
	std::wstring _score_message;

	glm::ivec2 _lives_message_location;
	glm::ivec2 _score_message_location;
};