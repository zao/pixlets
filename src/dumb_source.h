#pragma once

#include <gorilla/gau.h>

#include <memory>
#include <string>
#include "audio.h"
#include "vfs.h"

namespace dumb
{
void init();
void terminate();
std::shared_ptr<ga_Handle> load_xm(vfs::FilePath filepath, bool loop);
}