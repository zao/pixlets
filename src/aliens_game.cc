char const* const PRODUCT_NAME = "Aliens!";
char const* const PRODUCT_VERSION = "v0.0.7";

#include "aliens_game.h"
#include "audio.h"
#include "bitmap_font.h"
#include "bmfont.h"
#include "error_context.h"
#include "event_queue.h"
#include "game_screen.h"
#include "pix.h"
#include "sprite_sheet.h"
#include "vfs.h"

#include <algorithm>
#include <list>
#include <set>
#include <functional>

namespace stdph = std::placeholders;

#include "dumb_source.h"

bool input_queue_key_handler(EventQueue* queue, int key, int scancode, int action, int mods)
{
	KeyboardEvent evt = { key, scancode, action, mods };
	queue->push(KeyboardEvent::ID, evt);
	return true;
}

bool PlayerIntent::interpret_key(int key, int action)
{
	auto latch_key = [&](bool& target) {
		if (action == GLFW_PRESS)   target = true;
		if (action == GLFW_RELEASE) target = false;
	};
	auto trigger = [&](bool& target) {
		if (action == GLFW_PRESS)   target = true;
	};
	switch (key) {
		case GLFW_KEY_A:
		case GLFW_KEY_LEFT: {
			latch_key(want_move_left);
			return true;
		}
		case GLFW_KEY_D:
		case GLFW_KEY_RIGHT: {
			latch_key(want_move_right);
			return true;
		}
		case GLFW_KEY_SPACE: {
			trigger(want_shoot);
			return true;
		}
		case GLFW_KEY_F5: {
			trigger(cheat_decimate);
			return true;
		}
	}
	return false;
}

PlayScreen::PlayScreen(EventQueue* input_queue, CommonArtAssets* common_art, GameArtAssets* game_art, pix::SimulationParameters sim_params)
: _input_queue(input_queue)
, _art(game_art)
, _common_art(common_art)
, _sim_params(sim_params)
{
	_game.reset_game();
	if (_snd.intro_music) {
		get_audio().play_handle(_snd.intro_music.get());
	}
	auto message_h = sim_params.playfield_height + 1 - common_art->font->baseline_shift;
	_lives_message_location = glm::ivec2{50, message_h};
	_score_message_location = glm::ivec2{sim_params.playfield_width/2, message_h};

	sim_ticker = std::bind(&PlayScreen::sim_tick, this, stdph::_1, stdph::_2);
	vis_ticker = std::bind(&PlayScreen::vis_tick, this);
}

void PlayScreen::sim_tick(float /*dt*/, float t)
{
	while (!_input_queue->empty()) {
		EventHeader hdr = _input_queue->peek();
		if (hdr.id == KEYBOARD_EVENT) {
			KeyboardEvent evt;
			_input_queue->pop(evt);
			if (evt.key == GLFW_KEY_ESCAPE) {
				pix::flag_stop_running();
			}
			else {
				_game.intent.interpret_key(evt.key, evt.action);
			}
		}
		else {
			_input_queue->pop();
		}
	}
	++_game.frames_in_current_phase;
	switch (_game.game_phase) {
		case GamePhase::GAME_STARTING: {
			if (_game.frames_in_current_phase == 1) {
				_game.reset_board();
				_game.player_visible = false;
				if (has_audio())
					get_audio().play_handle(_snd.intro_music.get());
			}
			if (_game.frames_in_current_phase == GAME_START_DURATION) {
				if (has_audio())
					get_audio().play_handle(_snd.bgm_music.get());
				_game.change_phase(GamePhase::LIFE_STARTING);
			}
			else {
				break;
			}
		}
		case GamePhase::LIFE_STARTING: {
			_game.reset_player();
			_game.player_visible = true;
			_game.change_phase(GamePhase::GAME_RUNNING);
			// fall-through
		}
		case GamePhase::GAME_RUNNING: {
			std::list<std::list<Bullet>::iterator> perished_bullets;
			std::set<std::list<Alien>::iterator> perished_aliens;
			std::list<Bullet> new_bullets;
			bool player_death = false;

			auto doom_foe = [&](std::list<Alien>::iterator I){
				perished_aliens.insert(I);
			};

			auto& intent = _game.intent;
			auto& aliens = _game.aliens;
			auto& barriers = _game.barriers;
			auto& bullets = _game.bullets;
			auto& player = _game.player;
			if (intent.cheat_decimate && !aliens.empty()) {
				size_t n = aliens.size() / 10;
				while (perished_aliens.size() < n) {
					int k = rand() % aliens.size();
					auto I = aliens.begin();
					std::advance(I, k);
					doom_foe(I);
				}
				intent.cheat_decimate = false;
			}
			if (intent.want_move_left != intent.want_move_right) {
				if (intent.want_move_left)  _game.player.position.x -= 1;
				if (intent.want_move_right) _game.player.position.x += 1;
				_game.player.position.x = std::max(0, std::min(sim_params.playfield_width-PLAYER_SIZE.x, _game.player.position.x));
			}
			if (intent.want_shoot) {
				_game.shot_intent_time = t;
				_game.shot_requested = true;
				intent.want_shoot = false;
			}

			if (_game.shot_requested) {
				if (t - _game.shot_intent_time > SHOT_TIMING_TOLERANCE) {
					_game.shot_requested = false;
				}
				else if (!_game.player_bullet_exists()) {
					Bullet b = _game.make_player_bullet(_game.player.position + TURRET_OFFSET);
					new_bullets.push_back(b);
					_game.last_shot_time = t;
					_game.shot_requested = false;
					play_bite(Sound::PLAYER_PEW);
				}
			}

			if (_game.frames_to_next_alien_shot >= 0) {
				--_game.frames_to_next_alien_shot;
			}
			else {
				if (_game.enemy_bullet_count() < 3 && !aliens.empty()) {
					auto n = aliens.size();
					auto k = rand() % n;
					auto I = aliens.begin();
					std::advance(I, k);
					Bullet b = _game.make_enemy_bullet(I->position);
					new_bullets.push_back(b);
					_game.frames_to_next_alien_shot = _game.random_alien_shot_delay();
					play_bite(Sound::FOE_PEW);
				}
			}

			auto& alien_row = _game.alien_row;
			++_game.alien_idle_counter;				
			int const TICKS_PER_STEP = std::max<int>(2, aliens.size() / 10);
			while (_game.alien_idle_counter >= TICKS_PER_STEP) {
				++_game.alien_steps_taken;
				_game.alien_idle_counter -= TICKS_PER_STEP;
				if (_game.alien_steps_taken % STEPS_PER_ROW == 0) {
					++alien_row;
					//std::cerr << "Alien row: " << alien_row << std::endl;
					for (auto& alien : aliens) alien.position.y -= ALIEN_STRIDE.y/2;
				}
				else {
					auto alien_side_step = (alien_row%2) ? 1 : -1;
					for (auto& alien : aliens) alien.position.x += alien_side_step;
				}
				for (auto& alien : aliens) {
					for (auto& barrier : barriers) {
						if (barrier.health > 0 && collides(alien, barrier)) {
							--barrier.health;
						}
					}
				}
			}

			for (auto I = bullets.begin(); I != bullets.end(); ++I) {
				auto& bullet = *I;
				bullet.position += bullet.velocity;
				bool out_of_bounds =
					bullet.position.y > sim_params.playfield_height ||
					bullet.position.y-BULLET_SIZE.y < 0;
				bool consumed = false;
				if (bullet.owner == Owner::PLAYER) {
					for (auto aI = aliens.begin(); aI != aliens.end(); ++aI) {
						auto& alien = *aI;
						if (collides(bullet, alien) && !perished_aliens.count(aI)) {
							consumed = true;
							perished_aliens.insert(aI);
							break;
						}
					}
				}
				else if (bullet.owner == Owner::FOE) {
					if (collides(bullet, player)) {
						consumed = true;
						player_death = true;
					}
				}
				if (!consumed) {
					for (auto& barrier : barriers) {
						if (barrier.health > 0 && collides(bullet, barrier)) {
							play_bite(Sound::BARRIER_HIT);
							consumed = true;
							--barrier.health;
						}
					}
				}
				if (out_of_bounds || consumed) {
					perished_bullets.push_back(I);
				}
			}

			barriers.remove_if([&](Barrier b){
				bool dead = b.health <= 0;
				if (dead) {
					play_bite(Sound::BARRIER_DEATH);
				}
				return dead;
			});
			for (auto I : perished_aliens) {
				play_bite(Sound::FOE_DEATH);
				_game.add_score(alien_value(I->kind));
				aliens.erase(I);
			}
			for (auto I : perished_bullets) bullets.erase(I);
			bullets.insert(bullets.end(), new_bullets.begin(), new_bullets.end());

			if (player_death) {
				_game.change_phase(GamePhase::PLAYER_DEATH);
			}
			if (aliens.empty()) {
				_game.change_phase(GamePhase::GAME_WON);
			}
			break;
		}
		case GamePhase::PLAYER_DEATH: {
			play_bite(Sound::PLAYER_DEATH);
			_game.player_visible = false;
			--_game.lives;
			if (_game.lives == 0) {
				_game.change_phase(GamePhase::GAME_LOST);
			}
			else {
				_game.change_phase(GamePhase::RESPAWN_BREATHER);
			}
			break;
		}
		case GamePhase::RESPAWN_BREATHER: {
			if (_game.frames_in_current_phase == RESPAWN_BREATHER_DURATION) {
				_game.change_phase(GamePhase::LIFE_STARTING);
			}
			break;
		}
		case GamePhase::GAME_WON: {
			message_sprite = _art->win_message;
			play_bite(Sound::GAME_WON);
			break;
		}
		case GamePhase::GAME_LOST: {
			message_sprite = _art->lose_message;
			play_bite(Sound::GAME_LOST);
			break;
		}
	}
}

void PlayScreen::vis_tick()
{
	{
		std::wostringstream wos;
		wos << "Lives: " << _game.lives;
		_lives_message = wos.str();
	}
	{
		std::wostringstream wos;
		wos << "Score: " << _game.score;
		_score_message = wos.str();
	}
	pix::bitmap_font::draw_string(*_common_art->font, _lives_message, _lives_message_location);
	pix::bitmap_font::draw_string(*_common_art->font, _score_message, _score_message_location);
	for (auto bullet : _game.bullets)
		pix::mega_sprite::draw(*_art->shot_sprite, bullet.position);
	if (_game.player_visible) {
		pix::mega_sprite::draw(*_art->player_sprite, _game.player.position);
	}
	for (auto alien : _game.aliens)
		pix::mega_sprite::draw(*_art->alien_sprites[alien.kind], alien.position);
	for (auto barrier : _game.barriers)
		pix::mega_sprite::draw(*_art->barrier_sprite, barrier.position);
	if (message_sprite) {
		pix::mega_sprite::draw(*message_sprite, glm::ivec2{0, 0});
	}
}

struct Space
{
	virtual ~Space() {}
	virtual void update(float /*dt*/, float /*t*/) {}
	virtual void draw() {}

	void show() {do_show(); _show = true;}
	void hide() {do_hide(); _show = false;}

	bool shown() const {return _show;}
	bool hidden() const {return !_show;}

protected:
	virtual void do_hide() {}
	virtual void do_show() {}

	bool _show = false;
};

struct MenuSpace : Space
{
	MenuSpace(EventQueue* input_queue, CommonArtAssets* common_art, GameArtAssets* game_art, pix::SimulationParameters sim_params)
	: _input_queue(input_queue), _common_art(common_art), _art(game_art), _sim_params(sim_params)
	{
	}

	virtual void update(float /*dt*/, float /*t*/)
	{
		if (hidden())
			return;
		while (!_input_queue->empty()) {
			EventHeader hdr = _input_queue->peek();
			if (hdr.id == KEYBOARD_EVENT) {
				KeyboardEvent evt;
				_input_queue->pop(evt);
				auto* page = _page_stack.top();
				auto& selection = _page_selection.top();
				if (evt.action == GLFW_PRESS && evt.key == GLFW_KEY_ENTER) {
					if (selection >= 0) {
						auto& item = page->selectable_item(selection);
						item.action();
					}
				}
				else if (evt.action == GLFW_PRESS && (evt.key == GLFW_KEY_UP || evt.key == GLFW_KEY_W)) {
					if (page->has_selectables()) {
						int n = page->num_selectables();
						selection = (selection + n - 1) % n;
					}
				}
				else if (evt.action == GLFW_PRESS && (evt.key == GLFW_KEY_DOWN || evt.key == GLFW_KEY_S)) {
					if (page->has_selectables()) {
						int n = page->num_selectables();
						selection = (selection + 1) % n;
					}
				}
				else if (evt.action == GLFW_PRESS && evt.key == GLFW_KEY_ESCAPE) {
					if (_page_stack.size() > 1) {
						leave_page();
					}
					else {
						pix::flag_stop_running();
					}
				}
			}
			else {
				_input_queue->pop();
			}
		}
	}

	virtual void draw()
	{
		if (hidden())
			return;
		auto page = _page_stack.top();
		auto selection = _page_selection.top();
		int i = 0;
		for (auto& item : page->items) {
			pix::bitmap_font::draw_string(*_common_art->font, item.text, item.position);
			if (i == selection) {
				pix::mega_sprite::draw(*_art->foo_sprite, item.position + glm::ivec2{-16, 0});
			}
			++i;
		}
	}

	struct Item
	{
		std::wstring text;
		glm::ivec2 position;
		std::function<void ()> action;
		bool selectable;
	};

	struct Page
	{
		std::vector<Item> items;
		int selected_item;

		bool has_selectables() const
		{
			for (auto& item : items) {
				if (item.selectable) {
					return true;
				}
			}
			return false;
		}

		int num_selectables() const
		{
			int num = 0;
			for (auto& item : items) {
				if (item.selectable) {
					++num;
				}
			}
			return num;
		}

		Item& selectable_item(int id)
		{
			for (auto& item : items) {
				if (item.selectable) {
					if (id-- == 0)
						return item;
				}
			}
			PIX_CRASH("no selectable objects in menu");
		}
	};

	void enter_page(util::StringToken page_name)
	{
		auto I = _pages.find(page_name);
		assert(I != _pages.end());
		auto& page = I->second;
		if (page.has_selectables()) {
			_page_selection.push(0);
		}
		else {
			_page_selection.push(-1);
		}
		_page_stack.push(&page);
	}

	void leave_page()
	{
		assert(!_page_stack.empty());
		_page_stack.pop();
		_page_selection.pop();
	}

	EventQueue* _input_queue;
	CommonArtAssets* _common_art;
	GameArtAssets* _art;
	pix::SimulationParameters _sim_params;
	std::map<util::StringToken, Page> _pages;
	std::stack<Page*> _page_stack;
	std::stack<int> _page_selection;
};

struct PlaySpace : Space
{
	PlaySpace(EventQueue* input_queue, CommonArtAssets* common_art, GameArtAssets* game_art, pix::SimulationParameters sim_params)
	: _screen(input_queue, common_art, game_art, sim_params)
	{}

	virtual void update(float dt, float t)
	{
		if (shown()) {
			_screen.sim_ticker(dt, t);
		}
		else {
			_last_hidden = t;
		}
	}

	virtual void draw()
	{
		if (shown()) {
			_screen.vis_ticker();
		}
	}

	PlayScreen _screen;
	float _last_hidden;
};

static std::pair<std::vector<std::wstring>, std::vector<std::wstring>> credits_lines();

int main(int argc, char** /*argv*/)
{
	std::cerr << PRODUCT_NAME << " " << PRODUCT_VERSION << std::endl;
	bool audio_disabled = false;
	if (argc > 1) {
		std::cerr << "Disabling audio per request.\n";
		audio_disabled = true;
	}

	pix::WindowParameters win_params;
	win_params.width = 1280;
	win_params.height = 720;
	win_params.title = "Aliens!";

	if (!audio_disabled) {
		setup_audio();
	}
	pix::init(win_params, sim_params);
	{
		EventQueue input_queue;
		pix::register_keyboard_handler(
			std::bind(&input_queue_key_handler, &input_queue,
				stdph::_1, stdph::_2, stdph::_3, stdph::_4));
		/*
		auto olu = gl::meta_program::make_from_shader_sources(
			"#version 120\n"
			"uniform mat4 transform;\n"
			"void main()\n"
			"{\n"
			"    gl_FrontColor = gl_Color;\n"
			"        gl_Position = gl_ModelViewProjectionMatrix * transform * gl_Vertex;\n"
			"}\n"
			,
			"#version 120\n"
			"void main()\n"
			"{\n"
			"	gl_FragColor = gl_Color;\n"
			"}\n"
		);
		*/
		pix::register_material(0, 0, 127,
			gl::meta_program::make_from_shader_sources(
				*vfs::slurp_file("/shaders/megapixel-vs.glsl"),
				*vfs::slurp_file("/shaders/megapixel_inverse-fs.glsl")));
		pix::register_material(0, 0, 0,
			gl::meta_program::make_from_shader_sources(
				*vfs::slurp_file("/shaders/megapixel-vs.glsl"),
				*vfs::slurp_file("/shaders/megapixel-fs.glsl")));

		CommonArtAssets common_art;
		GameArtAssets game_art;

		MenuSpace menu_space(&input_queue, &common_art, &game_art, sim_params);
		PlaySpace play_space(&input_queue, &common_art, &game_art, sim_params);

		std::vector<Space*> spaces;
		spaces.push_back(&menu_space);
		spaces.push_back(&play_space);

		{
			auto& root = menu_space._pages[util::string_token("root")];
			root.items.push_back({
				L"Start game", glm::ivec2{30, 60}, [&] {
					menu_space.hide();
					play_space.show();
				}, true });
			root.items.push_back({
				L"Credits", glm::ivec2{30, 45}, [&] {
					menu_space.enter_page(util::string_token("credits"));
				}, true });
			root.items.push_back({
				L"Exit game", glm::ivec2{30, 30}, [&] {
					pix::flag_stop_running();
				}, true});
		}
		{
			auto& credits = menu_space._pages[util::string_token("credits")];
			glm::ivec2 credits_pos{35, 120};
			for (auto line : credits_lines().first) {
				if (!line.empty()) {
					credits.items.push_back({
						line, credits_pos, []{}, false });
				}
				credits_pos -= glm::ivec2{0, 12};
			}
			credits_pos = glm::ivec2{130, 120};
			for (auto line : credits_lines().second) {
				if (!line.empty()) {
					credits.items.push_back({
						line, credits_pos, []{}, false });
				}
				credits_pos -= glm::ivec2{0, 12};
			}
		}
		menu_space.enter_page(util::string_token("root"));

		menu_space.show();
		while (pix::running()) {
			(void)pix::sim_tick([&](float dt, float t){
				for (auto space : spaces) {
					space->update(dt, t);
				}
			});
			if (has_audio())
				get_audio().update();
			pix::draw_tick([&]{
				for (auto space : spaces) {
					space->draw();
				}
			});
		}
	}
	pix::terminate();
	dumb::terminate();
}

void PlayScreen::play_bite(Sound key)
{
	Audio::Sound* sound = nullptr;
	switch (key) {
		case Sound::PLAYER_PEW: {
			sound = _snd.pew_sound.get();
			break;
		}
		case Sound::PLAYER_DEATH: {
			sound = _snd.death_sound.get();
			break;
		}
		case Sound::FOE_PEW: {
			sound = _snd.alien_pew_sound.get();
			break;
		}
		case Sound::FOE_DEATH: {
			sound = _snd.alien_die_sound.get();
			break;
		}
		case Sound::BARRIER_HIT: {
			sound = _snd.barrier_hit_sound.get();
			break;
		}
		case Sound::BARRIER_DEATH: {
			sound = _snd.barrier_death_sound.get();
			break;
		}
		default:
			break;
	}
	if (sound) {
		get_audio().play_sound(sound);
	}
}

GameArtAssets::GameArtAssets()
{
	win_message = pix::mega_sprite::load("/aliens/space_win", "png", false);
	lose_message = pix::mega_sprite::load("/aliens/space_lose", "png", false);
	player_sprite = pix::mega_sprite::load("/aliens/player_ship", "png");
	shot_sprite = pix::mega_sprite::load("/aliens/simple_beam", "png");
	barrier_sprite = pix::mega_sprite::load("/aliens/barrier", "png");
	foo_sprite = pix::mega_sprite::load("/aliens/alien_foo", "png");
	bar_sprite = pix::mega_sprite::load("/aliens/alien_bar", "png");
	baz_sprite = pix::mega_sprite::load("/aliens/alien_baz", "png");
	alien_sprites[0] = foo_sprite;
	alien_sprites[1] = bar_sprite;
	alien_sprites[2] = baz_sprite;
}

GameSoundAssets::GameSoundAssets()
{
	if (has_audio()) {
		dumb::init();
		intro_music = dumb::load_xm("/intro.xm", false);
		bgm_music = dumb::load_xm("/bgm/ambient.xm", true);

		pew_sound = get_audio().load_sound("/sfx/pew", "wav");
		death_sound = get_audio().load_sound("/sfx/ship_death", "wav");
		alien_die_sound = get_audio().load_sound("/sfx/alien_death", "wav");
		alien_pew_sound = get_audio().load_sound("/sfx/alien_pew", "wav");
		barrier_hit_sound = get_audio().load_sound("/sfx/barrier_hit", "wav");
		barrier_death_sound = get_audio().load_sound("/sfx/barrier_death", "wav");
	}
}

CommonArtAssets::CommonArtAssets()
{
	bmf::BMFont font_source = bmf::bmfont::load("/fonts/verdana-12.fnt");
	font = pix::bitmap_font::make_from_bmfont(font_source);
}

static std::pair<std::vector<std::wstring>, std::vector<std::wstring>> credits_lines()
{
	return {
		{
			L"Code:",
			L"  zao",
			L"Graphics:",
			L"  zao",
			L"Gameplay:",
			L"  zao",
			L"Sound effects:",
			L"  zao",
			L"Music:",
			L"  zao"
		},{
			L"Libraries used:",
			L"- Boost 1.54.0",
			L"- DUMB trunk",
			L"- GLEW 1.10.0",
			L"- GLFW 3.0.1",
			L"- GLM 0.9.4.4",
			L"- GorillaAudio 0.3.0",
			L"- json-cpp trunk",
			L"- stb_image 1.33",
		}
	};
}