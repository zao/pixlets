#include "string_token.h"
#include <iostream>
#include <unordered_map>
#include <vector>

namespace util
{
typedef uint64_t StringToken;
static std::unordered_map<std::string, StringToken> g_token_store;
static std::vector<std::string> g_string_store;
static uint64_t g_token_source = 0;

// thread-unsafe
StringToken string_token(std::string const& s)
{
	auto I = g_token_store.find(s);
	if (I != g_token_store.end()) {
		return I->second;
	}
	StringToken token = g_token_source++;
	g_string_store.push_back(s);
	return g_token_store[s] = token;
}

std::string redeem_token(StringToken token)
{
	std::string s = g_string_store[token];
	return s;
}
}