#pragma once

#if !defined(PIX_USE_GORILLA_AUDIO)
#  define PIX_USE_GORILLA_AUDIO 1
#endif

#include <boost/noncopyable.hpp>
#include <memory>

#if PIX_USE_GORILLA_AUDIO
#include <gorilla/gau.h>
#endif

#if PIX_USE_GORILLA_AUDIO
struct Audio : boost::noncopyable
{
	typedef ga_Sound Sound;
	typedef ga_Handle Handle;
	typedef std::shared_ptr<Sound> SoundHandle;
	typedef std::shared_ptr<Handle> HandleHandle;

	Audio();
	~Audio();

	void update();

	SoundHandle load_sound(std::string const& path, std::string const& type);
	void play_sound(Sound* sound);
	void play_handle(Handle* handle);


//private:
	bool _enabled;
	gau_Manager* _mgr;
	ga_Mixer* _mixer;
	ga_StreamManager* _stream_mgr;
};
#else
struct Audio
{
	typedef void Sound;
	typedef void Handle;
	typedef std::shared_ptr<Sound> SoundHandle;
	typedef std::shared_ptr<Handle> HandleHandle;

	Audio();
	~Audio();

	void update();

	SoundHandle load_sound(std::string const& /*path*/, std::string const& /*type*/);
	void play_sound(Sound* /*sound*/);
	void play_handle(Handle* /*handle*/);
};
#endif

void setup_audio();
void shutdown_audio();
bool has_audio();
Audio& get_audio();