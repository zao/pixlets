#pragma once
#include "json/json.h"
#include "pix.h"
#include "string_token.h"
#include "vfs.h"

#include <memory>

namespace pix
{
struct SpriteSheet
{
	struct Entry
	{
		util::StringToken name;
		glm::ivec2 pos, size;
		std::shared_ptr<MegaSprite> sprite;
	};
	std::vector<Entry> entries;
};

namespace sprite_sheet
{
	std::shared_ptr<SpriteSheet> load_from_json_value(Json::Value const& v);
	std::shared_ptr<SpriteSheet> load_from_json_memory(char const* p, size_t n);
	std::shared_ptr<SpriteSheet> load_from_json_file(vfs::FilePath filepath);
	void draw(SpriteSheet const& sheet, util::StringToken name, glm::ivec2 pos);
}
}