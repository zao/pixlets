#include "vfs.h"
#include <fstream>
#include <iostream>
#include <vector>

namespace vfs
{
std::unique_ptr<std::string> slurp_file(std::string filepath)
{
	std::ifstream is("content" + filepath, std::ios::binary);
	if (!is) {
		std::cerr << "Failed to open file " << filepath << std::endl;
		return nullptr;
	}
	is.seekg(0, std::ios::end);
	auto num_bytes = is.tellg();
	if (num_bytes == 0) {
		std::cerr << "File empty: " << filepath << std::endl;
		return std::unique_ptr<std::string>(new std::string());
	}
	is.seekg(0, std::ios::beg);
	std::vector<char> data(num_bytes);
	is.read(data.data(), data.size());
	return std::unique_ptr<std::string>(new std::string(data.begin(), data.end()));
}

FilePath truncate_leaf(FilePath source)
{
	auto off = source.find_last_of('/');
	return source.substr(0, off+1);
}

std::string remove_extension(FilePath& source)
{
	auto off = source.find_last_of('.');
	auto ext = source.substr(off+1);
	source = source.substr(0, off);
	return ext;
}
}