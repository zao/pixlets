#include "pix.h"
#include "stb_image.h"
#include "vfs.h"

#include <list>
#include <string>

#include "json/json.h"

namespace pix
{
std::map<MaterialCode, MaterialToken> g_material_table;
std::vector<std::unique_ptr<gl::MetaProgram>> g_material_programs;
std::unique_ptr<gl::IndexedModel> g_pixel_model;

struct ViewParameters
{
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;

	int left_margin, right_margin;
	int top_margin, bottom_margin;
};

static WindowParameters g_window_params;
static SimulationParameters g_simulation_params;
static ViewParameters g_view_params;
static bool g_running = true;
static GLFWwindow* g_wnd = nullptr;

int screen_width() {return g_window_params.width;}
int screen_height() {return g_window_params.height;}
std::string window_title() {return g_window_params.title;}
bool running() {return g_running;}
void flag_stop_running() {g_running = false;}

typedef std::function<bool (int /*key*/, int /*scancode*/, int /*action*/, int /*mods*/)> KeyboardHandler;
static std::list<KeyboardHandler> g_keyboard_handlers;
void register_keyboard_handler(KeyboardHandler handler)
{
	g_keyboard_handlers.push_back(handler);
}

static void dispatch_keyboard_handler(int key, int scancode, int action, int mods)
{
	for (auto& handler : g_keyboard_handlers) {
		if (handler(key, scancode, action, mods))
			return;
	}
}

void register_material(uint8_t r, uint8_t g, uint8_t b, std::unique_ptr<gl::MetaProgram>&& program)
{
	g_material_table[MaterialCode(r, g, b)] = g_material_programs.size();
	g_material_programs.emplace_back(program.release());
}

MaterialToken material_token(uint8_t r, uint8_t g, uint8_t b)
{
	auto tup = std::tie(r, g, b);
	auto I = g_material_table.find(tup);
	if (I != g_material_table.end()) {
		return I->second;
	}
	char buf[32] = {};
	sprintf(buf, "(%d,%d,%d)", r, g, b);
	ErrorContext("doing material lookup", buf);
	crash(__FILE__, __LINE__, "material not found");
}

gl::MetaProgram const& resolve_material(MaterialToken tok)
{
	assert (tok < g_material_programs.size());
	return *g_material_programs[tok];
}

Batcher::Batcher()
: material_buckets(g_material_programs.size())
{}

namespace rgba_image
{
bool load(std::string filepath, RGBAImage& out)
{
	assert(!out._bitmap);
	auto file_contents = vfs::slurp_file(filepath);
	int width = 0, height = 0, comp = 0;
	auto* ptr = (stbi_uc const*)file_contents->data();
	out._bitmap = stbi_load_from_memory(ptr, file_contents->size(), &width, &height, &comp, 4);
	if (!out._bitmap) {
		return false;
	}
	std::tie(out._width, out._height, out._comp) = std::tie(width, height, comp);
	return true;
}
}

namespace mega_sprite
{
static std::unique_ptr<MegaSprite> load_impl(vfs::FilePath filepath, std::string extension, bool load_material, bool decorate_filepaths)
{
	RGBAImage color_image, material_image;
	vfs::FilePath color_filepath, material_filepath;
	if (decorate_filepaths) {
		color_filepath = filepath + "-color." + extension;
		if (load_material) {
			material_filepath = filepath + "-material." + extension;
		}
	}
	else {
		color_filepath = filepath;
	}
	if (!rgba_image::load(color_filepath, color_image)) {
		std::cerr << "Could not load color image for " << color_filepath << std::endl;
		return nullptr;
	}
	if (load_material && !rgba_image::load(material_filepath, material_image)) {
		std::cerr << "Could not load material image for " << material_filepath << std::endl;
		return nullptr;
	}
	if (load_material && color_image.dimensions() != material_image.dimensions()) {
		std::cerr << "Image dimension mismatch for " << filepath << std::endl;
		return nullptr;
	}
	auto width = color_image._width;
	auto height = color_image._height;
	std::unique_ptr<MegaSprite> ret(new MegaSprite(width, height));
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			auto* pix = color_image.pixel(x, y);
			ret->pixel(x, y).color = glm::vec4(pix[0]/255.0f, pix[1]/255.0f, pix[2]/255.0f, pix[3]/255.0f);
			if (load_material) {
				auto* mpix = material_image.pixel(x, y);
				if (mpix[3]) {
					ret->pixel(x, y).material = material_token(mpix[0], mpix[1], mpix[2]);
				}
			}
		}
	}
	return ret;
}
std::unique_ptr<MegaSprite> load(vfs::FilePath filepath_base, std::string extension, bool load_material)
{
	return load_impl(filepath_base, extension, load_material, true);
}
std::unique_ptr<MegaSprite> load(vfs::FilePath filepath)
{
	return load_impl(filepath, "", false, false);
}

void draw(MegaSprite const& sprite, glm::ivec2 shift)
{
	sprite.draw(shift);
}

void draw(MegaSprite const& sprite, glm::ivec2 shift, MegaSprite::Rect subset)
{
	sprite.draw(shift, subset);
}
}

RGBAImage::~RGBAImage()
{
	if (_bitmap) {
		stbi_image_free(_bitmap);
	}
}

template <typename Renderables>
void draw_pixels(gl::MetaProgram const& program, Renderables const& renderables)
{
	static util::StringToken
		mat_view          = util::string_token("mat_view"),
		mat_projection    = util::string_token("mat_projection");
	auto& prog = program;
	gl::ScopedUseProgram sup(prog.program());
	gl::meta_program::set_uniform(prog, mat_view,       g_view_params.view_matrix);
	gl::meta_program::set_uniform(prog, mat_projection, g_view_params.projection_matrix);
	auto& m = *g_pixel_model;
	auto num_buffers = m.num_buffers();
	gl::BufferHandle instances;
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m.elements().id());
	auto bindings = m.bindings();
	{
		gl::fill_vbo(instances.id(), GL_DYNAMIC_DRAW, renderables);
		GLuint aux_buffers[] = { instances.id(), instances.id() };
		for (auto program_loc : prog.attribs()) {
			auto sbI = m.bindings().find(program_loc.first);
			if (sbI != m.bindings().end()) {
				auto& sb = sbI->second;
				GLuint vbo = 0u;
				if (sb._vbo_index < num_buffers) {
					vbo = m.buffer(sb._vbo_index).id();
				}
				else {
					vbo = aux_buffers[sb._vbo_index - num_buffers];
				}
				glBindBuffer(GL_ARRAY_BUFFER, vbo);
				if (sb._type == GL_INT) {
					glVertexAttribIPointer(program_loc.second, sb._count, sb._type,
						sb._stride, sb._offset);
				}
				else {
					glVertexAttribPointer(program_loc.second, sb._count, sb._type,
						sb._normalized, sb._stride, sb._offset);	
				}
				glVertexAttribDivisor(program_loc.second, sb._divisor);
				glEnableVertexAttribArray(program_loc.second);
			}
		}
		glDrawElementsInstanced(m.primitive_type(), m.element_count(), m.element_type(), 0, renderables.size());
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void MegaSprite::draw(glm::ivec2 offset) const
{
	Rect r = { glm::ivec2{0, 0}, glm::ivec2{_cols, _rows} };
	draw(offset, r);
}

void MegaSprite::draw(glm::ivec2 offset, MegaSprite::Rect subset) const
{
	Batcher b;
	glm::ivec2 dims = subset.max - subset.min;
	for (int row = 0; row < dims.y; ++row) {
		for (int col = 0; col < dims.x; ++col) {
			auto loc = subset.min + glm::ivec2{col, row};
			auto& pix = pixel(loc.x, loc.y);
			if (pix.color.a > 0.0f) {
				auto c = pix.color * pix.color.a;
				c.a = pix.color.a;
				glm::ivec2 pixel_pos = offset + glm::ivec2(col, dims.y - row - 1);
				b.material_buckets[pix.material].push_back({pixel_pos, c});
			}
		}
	}
	for (size_t i = 0; i < b.material_buckets.size(); ++i) {
		if (!b.material_buckets[i].empty()) {
			auto& prog = resolve_material(i);
			draw_pixels(prog, b.material_buckets[i]);
		}
	}
}

namespace indexed_model
{
std::unique_ptr<gl::IndexedModel> make_pixel_model()
{
	std::unique_ptr<gl::IndexedModel> ret(new gl::IndexedModel);
	ret->_element_count = 6;
	ret->_element_type = GL_UNSIGNED_SHORT;
	ret->_primitive_type = GL_TRIANGLES;
	std::vector<uint16_t> elements_source = { 0, 1, 2, 0, 2, 3 };
	gl::fill_vbo(ret->_elements.id(), GL_STATIC_DRAW, elements_source);
	std::vector<glm::vec2> positions = {
		glm::vec2(0.0f, 0.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),
		glm::vec2(0.0f, 1.0f),
	};
	std::vector<glm::vec2> texcoords = {
		glm::vec2(0.0f, 0.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),
		glm::vec2(0.0f, 1.0f),
	};
	auto positions_holder = std::make_shared<gl::BufferHandle>();
	auto texcoords_holder = std::make_shared<gl::BufferHandle>();
	gl::fill_vbo(positions_holder->id(), GL_STATIC_DRAW, positions);
	gl::fill_vbo(texcoords_holder->id(), GL_STATIC_DRAW, texcoords);
	ret->_buffers.push_back(positions_holder);
	ret->_buffers.push_back(texcoords_holder);

	GLuint offset_stride = 16+8;
	GLuint color_stride = 16+8;
	auto offset_offset = (void*)0;
	auto color_offset = (void*)sizeof(glm::ivec2);
	// { name, type, count, vbo_index, normalized, stride, offset, divisor };
	ret->_bindings[util::string_token("vs_position")] = gl::stream_binding::make(
		"vs_position", GL_FLOAT, 2, 0, GL_FALSE, 0, nullptr,
		gl::PER_VERTEX_DIVISOR);
	ret->_bindings[util::string_token("vs_texcoord")] = gl::stream_binding::make(
		"vs_texcoord", GL_FLOAT, 2, 1, GL_FALSE, 0, nullptr,
		gl::PER_VERTEX_DIVISOR);
	ret->_bindings[util::string_token("inst_offset")] = gl::stream_binding::make(
		"inst_offset", GL_INT,   2, 2, GL_FALSE, offset_stride, offset_offset,
		gl::PER_INSTANCE_DIVISOR);
	ret->_bindings[util::string_token("inst_color")] = gl::stream_binding::make(
		"inst_color", GL_FLOAT,  4, 2, GL_FALSE, color_stride,  color_offset,
		gl::PER_INSTANCE_DIVISOR);
	return ret;
}
}

static void window_size_handler(GLFWwindow* /*wnd*/, int width, int height)
{
	g_window_params.width = width;
	g_window_params.height = height;
	int x_blocks = g_simulation_params.playfield_width;
	int y_blocks = g_simulation_params.playfield_height;
	double block_width  = (double)width  / (double)x_blocks;
	double block_height = (double)height / (double)y_blocks;
	double block_size = std::min(block_width, block_height);
	int viewport_width  = x_blocks*block_size;
	int viewport_height = y_blocks*block_size;

	g_view_params.projection_matrix = glm::ortho<float>(
		0.0f, x_blocks,
		0.0f, y_blocks);
	{
		auto x_shift = center_interval(viewport_width, width);
		auto y_shift = center_interval(viewport_height, height);
		g_view_params.left_margin = x_shift;
		g_view_params.right_margin = width - viewport_width;
		g_view_params.top_margin = width - viewport_height;
		g_view_params.bottom_margin = y_shift;
		glViewport(x_shift, y_shift, viewport_width, viewport_height);
	}
}

static double const TURBO_BOOSTS[] = { 0.0, 0.5, 1.0, 9.0 };
static double g_turbo_boost = 0.0;

static void keyboard_handler(GLFWwindow* /*wnd*/, int key, int scancode, int action, int mods)
{
	switch (key) {
		case GLFW_KEY_F5: {
			if (action == GLFW_PRESS && (mods & GLFW_MOD_SHIFT)) {
				pix::flag_stop_running();
			}
			break;
		}
		case GLFW_KEY_F1:
		case GLFW_KEY_F2:
		case GLFW_KEY_F3:
		case GLFW_KEY_F4: {
			if (action == GLFW_PRESS) {
				int level = key - GLFW_KEY_F1;
				g_turbo_boost = TURBO_BOOSTS[level];
				std::cerr << "Turbo level " << (level+1) << "/4, " << (1+g_turbo_boost)*100 << "%\n";
			}
			break;
		}		
		default: {
			pix::dispatch_keyboard_handler(key, scancode, action, mods);
		}
	}
}

#if defined(_WIN32)
#define PIX_GL_CALLCONV __stdcall
#else
#define PIX_GL_CALLCONV
#endif
void PIX_GL_CALLCONV debug_callback_proc(GLenum source,
                                         GLenum type,
                                         GLuint id,
                                         GLenum severity,
                                         GLsizei length,
                                         const char* message,
                                         void* /*user_param*/)
{
	std::string s(message, message + length);
	fprintf(stderr, "DEBUG: %u %u %u %u %u %s\n", source, type, id, severity, length, s.c_str());
}

void init(WindowParameters const& win_params, SimulationParameters const& sim_params)
{
	ErrorContext ec("in pix init");
	g_window_params = win_params;
	g_simulation_params = sim_params;
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_FALSE);
	g_wnd = glfwCreateWindow(screen_width(), screen_height(), window_title().c_str(), nullptr, nullptr);
	std::cerr << "Major version: " << glfwGetWindowAttrib(g_wnd, GLFW_CONTEXT_VERSION_MAJOR) << std::endl;
	std::cerr << "Minor version: " << glfwGetWindowAttrib(g_wnd, GLFW_CONTEXT_VERSION_MINOR) << std::endl;
	std::cerr << "Debug context: " << glfwGetWindowAttrib(g_wnd, GLFW_OPENGL_DEBUG_CONTEXT) << std::endl;
	glfwMakeContextCurrent(g_wnd);
	glewInit();
	glGetError();
	if (glDebugMessageControlARB) {
		glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	if (glDebugMessageCallbackARB) {
		glDebugMessageCallbackARB(&debug_callback_proc, nullptr);
	}
	g_pixel_model = indexed_model::make_pixel_model();

	window_size_handler(g_wnd, screen_width(), screen_height());
	glfwSetKeyCallback(g_wnd, &keyboard_handler);
	glfwSetWindowSizeCallback(g_wnd, &window_size_handler);
}

void terminate()
{
	g_pixel_model.reset();
	g_material_programs.clear();
	g_keyboard_handlers.clear();
	glfwDestroyWindow(g_wnd);
	glfwTerminate();
}

static double last_sim_t = glfwGetTime();
static double g_turbo_add = 0.0;

double sim_tick(Ticker ticker)
{
	ErrorContext ec("in sim phase");
	double const dt = g_simulation_params.frame_duration;
	double t = glfwGetTime();
	double wall_dt = t + g_turbo_add - last_sim_t;
	g_turbo_add += wall_dt * g_turbo_boost;
	t += g_turbo_add;
	while (last_sim_t + dt < t) {
		ticker(dt, last_sim_t);
		last_sim_t += dt;
	}
	return t;
}

void draw_tick(Drawer drawer)
{
	ErrorContext ec("in draw phase");
	glClearColor(0.05, 0.1, 0.15, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	drawer();
	glfwSwapBuffers(g_wnd);
	glfwPollEvents();
}

void crash(char const* file, int line, char const* reason)
{
	print_error_context();
	fprintf(stderr, "Assertion failed: %s\n", reason);
	fprintf(stderr, "In %s:%d\n", file, line);
	abort();
}
}
