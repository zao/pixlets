#pragma once
#include <cstdint>
#include <string>

namespace util
{
typedef uint64_t StringToken;

// thread-unsafe
StringToken string_token(std::string const& s);
std::string redeem_token(StringToken token);
}