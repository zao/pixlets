#pragma once
#include <functional>

struct GameScreen
{
	std::function<void (float dt, float t)> sim_ticker = [](float,float){};
	std::function<void ()> vis_ticker = []{};
};