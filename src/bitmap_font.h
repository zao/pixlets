#pragma once
#include "pix.h"
#include <memory>
#include <utility>
#include <unordered_map>

namespace bmf
{
struct BMFont;
}

namespace pix
{
struct BitmapFont
{
	struct Character
	{
		MegaSprite const* sprite;
		MegaSprite::Rect sheet_region;
		glm::ivec2 origin_offset;
		int advance;

	};
	struct KerningInfo
	{
		int amount;
	};
	typedef std::unordered_map<uint32_t, Character> Characters;
	typedef std::unordered_map<uint32_t, std::unordered_map<uint32_t, KerningInfo>> KerningInfos;

	int baseline_shift;
	int line_height;

	Character const& get(uint32_t ch) const;
	int kern(uint32_t a, uint32_t b) const;

	std::vector<std::shared_ptr<MegaSprite>> _pages;
	Characters _characters;
	KerningInfos _kerning_infos;
};

namespace bitmap_font
{
std::shared_ptr<BitmapFont> make_from_bmfont(bmf::BMFont const& source);
void draw_string(BitmapFont const& font, std::wstring s, glm::ivec2 shift);
}
}