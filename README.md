Building dependencies
=====================

	pushd ~/bounce
	export PREFIX=/c/libs/gcc
	export PREFIX=${HOME}/libs/gcc
	
	## GLFW 3.0.1
	curl -OLJk https://sourceforge.net/projects/glfw/files/glfw/3.0.1/glfw-3.0.1.zip/download
	unzip glfw-3.0.1.zip
	pushd glfw-3.0.1
	mkdir build
	cd build
	cmake .. -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON
	make -j9 install
	popd
	
	## GLM 0.9.4.4
	curl -OLJk https://sourceforge.net/projects/ogl-math/files/glm-0.9.4.4/glm-0.9.4.4.zip/download
	unzip glm-0.9.4.4.zip
	pushd glm-0.9.4.4
	install -d ${PREFIX}/include
	cp -r glm ${PREFIX}/include/
	popd
	
	## Boost 1.54.0
	curl -OLJk https://sourceforge.net/projects/boost/files/boost/1.54.0/boost_1_54_0.tar.bz2/download
	tar xjf boost_1_54_0.tar.bz2
	pushd boost_1_54_0
	./bootstrap.sh
	./b2 toolset=gcc --prefix=${PREFIX} --layout=tagged install link=static -j9
	popd
	
	## GLEW 1.10.0
	# requires Xmu, Xi, GL, Xext, X11
	curl -OLJk https://sourceforge.net/projects/glew/files/glew/1.10.0/glew-1.10.0.tgz/download
	tar xzf glew-1.10.0.tgz
	pushd glew-1.10.0
	make install GLEW_DEST=${PREFIX}
	popd

	## GorillaAudio 0.3.0
	# requires OpenAL
	curl -OLJk https://gorilla-audio.googlecode.com/files/GorillaAudio-0.3.0-src.zip
	mkdir GorillaAudio-0.3.0
	pushd GorillaAudio-0.3.0
	unzip ../GorillaAudio-0.3.0-src.zip
	cd build

	# on Linux
	cmake .
	make -j9
	cp ../bin/linux/Release ${PREFIX}/lib/
	
	# on Windows
	# replace WIN32 for integer types with _MSC_VER in gc_types.h
	cmake -G "MinGW Makefiles" -DENABLE_OPENAL=1 -DENABLE_XAUDIO2=0
	mingw32-make
	cp ..\bin\Release\libgorilla.a ${PREFIX}/lib/

	cp -r ../include/gorilla/ ${PREFIX}/include/gorilla
	popd

	popd

	## DUMB (kode54 git)
	git clone https://bitbucket.org/kode54/dumb.git
