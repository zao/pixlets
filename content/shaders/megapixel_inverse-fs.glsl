#version 130

in vec2 fs_texcoord;
in vec3 fs_color;

out vec4 frag_color;

void main(void)
{
	vec2 centered = fs_texcoord - vec2(0.5, 0.5);
	float man_dist = max(abs(centered.x), abs(centered.y));
	float intensity = smoothstep(1.0, 0.0, man_dist);
	vec4 color = vec4(fs_color * vec3(intensity, intensity, intensity), 1.0);
	frag_color = color;
}