#version 130

uniform mat4 mat_view;
uniform mat4 mat_projection;
uniform vec4 vec_color;

in vec2 vs_position;
in vec2 vs_texcoord;
in vec4 inst_color;
in ivec2 inst_offset;

out vec2 fs_texcoord;
out vec3 fs_color;

void main(void)
{
	vec4 aug_pos = vec4(vs_position + inst_offset, 0.0, 1.0);
	gl_Position = mat_projection * mat_view * aug_pos;
	fs_texcoord = vs_texcoord;
	fs_color = inst_color.rgb;
}