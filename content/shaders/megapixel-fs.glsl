#version 130

in vec2 fs_texcoord;
in vec3 fs_color;

out vec4 frag_color;

void main(void)
{
	frag_color = vec4(fs_texcoord.x, fs_texcoord.y, 0.5, 1.0);
	float intensity = pow(mix(1.0, 0.0, clamp(length(fs_texcoord - vec2(0.5, 0.5)), 0.0, 1.0)), 4);
	vec4 color = vec4(fs_color * vec3(intensity, intensity, intensity), 1.0);
	frag_color = color;
}