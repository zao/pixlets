#!/usr/bin/env bash

PRODUCT=aliens-0.0.7
DEST_DIR=${PRODUCT}

rm -rf ${DEST_DIR}
mkdir -p ${DEST_DIR}/content/aliens
mkdir -p ${DEST_DIR}/content/bgm
mkdir -p ${DEST_DIR}/content/fonts
mkdir -p ${DEST_DIR}/content/sfx
mkdir -p ${DEST_DIR}/content/shaders
cp "content/aliens/alien_bar-color.png" ${DEST_DIR}/content/aliens
cp "content/aliens/alien_bar-material.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/alien_baz-color.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/alien_baz-material.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/alien_foo-color.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/alien_foo-material.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/barrier-color.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/barrier-material.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/player_ship-color.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/player_ship-material.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/simple_beam-color.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/simple_beam-material.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/space_lose-color.png" ${DEST_DIR}/content/aliens/
cp "content/aliens/space_win-color.png" ${DEST_DIR}/content/aliens/
cp "content/bgm/ambient.xm" ${DEST_DIR}/content/bgm/
cp "content/fonts/verdana-12.fnt" ${DEST_DIR}/content/fonts/
cp "content/fonts/verdana-12_0.png" ${DEST_DIR}/content/fonts/
cp "content/intro.xm" ${DEST_DIR}/content/
cp "content/sfx/alien_death.wav" ${DEST_DIR}/content/sfx/
cp "content/sfx/alien_pew.wav" ${DEST_DIR}/content/sfx/
cp "content/sfx/barrier_death.wav" ${DEST_DIR}/content/sfx/
cp "content/sfx/barrier_hit.wav" ${DEST_DIR}/content/sfx/
cp "content/sfx/pew.wav" ${DEST_DIR}/content/sfx/
cp "content/sfx/ship_death.wav" ${DEST_DIR}/content/sfx/
cp "content/shaders/megapixel-fs.glsl" ${DEST_DIR}/content/shaders/
cp "content/shaders/megapixel-vs.glsl" ${DEST_DIR}/content/shaders/
cp "content/shaders/megapixel_inverse-fs.glsl" ${DEST_DIR}/content/shaders/

cp t2-output/linux-gcc-production-default/aliens ${DEST_DIR}/

cp ${HOME}/libs/gcc/lib/libglfw.so.3 ${DEST_DIR}/
cp ${HOME}/libs/gcc/lib64/libGLEW.so.1.10 ${DEST_DIR}/

rm ${PRODUCT}.tar.bz2
tar cjf ${PRODUCT}.tar.bz2 ${DEST_DIR}
