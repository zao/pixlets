Build {
	Units = function ()
		local dumb = StaticLibrary {
			Name = "dumb",
			Sources = {
				"src/core/atexit.c",
				"src/core/duhlen.c",
				"src/core/duhtag.c",
				"src/core/dumbfile.c",
				"src/core/loadduh.c",
				"src/core/makeduh.c",
				"src/core/rawsig.c",
				"src/core/readduh.c",
				"src/core/register.c",
				"src/core/rendduh.c",
				"src/core/rendsig.c",
				"src/core/unload.c",
				"src/helpers/barray.c",
				"src/helpers/blip_buf.c",
				"src/helpers/clickrem.c",
				"src/helpers/fir_resampler.c",
				"src/helpers/lanczos_resampler.c",
				"src/helpers/lpc.c",
				"src/helpers/memfile.c",
				"src/helpers/resample.c",
				"src/helpers/riff.c",
				"src/helpers/sampbuf.c",
				"src/helpers/silence.c",
				"src/helpers/stdfile.c",
				"src/helpers/tarray.c",
				"src/it/itload.c",
				"src/it/itload2.c",
				"src/it/itmisc.c",
				"src/it/itorder.c",
				"src/it/itread.c",
				"src/it/itread2.c",
				"src/it/itrender.c",
				"src/it/itunload.c",
				"src/it/load669.c",
				"src/it/load6692.c",
				"src/it/loadamf.c",
				"src/it/loadamf2.c",
				"src/it/loadany.c",
				"src/it/loadany2.c",
				"src/it/loadasy.c",
				"src/it/loadasy2.c",
				"src/it/loadmod.c",
				"src/it/loadmod2.c",
				"src/it/loadmtm.c",
				"src/it/loadmtm2.c",
				"src/it/loadokt.c",
				"src/it/loadokt2.c",
				"src/it/loadoldpsm.c",
				"src/it/loadoldpsm2.c",
				"src/it/loadpsm.c",
				"src/it/loadpsm2.c",
				"src/it/loadptm.c",
				"src/it/loadptm2.c",
				"src/it/loadriff.c",
				"src/it/loadriff2.c",
				"src/it/loads3m.c",
				"src/it/loads3m2.c",
				"src/it/loadstm.c",
				"src/it/loadstm2.c",
				"src/it/loadxm.c",
				"src/it/loadxm2.c",
				"src/it/ptmeffect.c",
				"src/it/read669.c",
				"src/it/read6692.c",
				"src/it/readam.c",
				"src/it/readamf.c",
				"src/it/readamf2.c",
				"src/it/readany.c",
				"src/it/readany2.c",
				"src/it/readasy.c",
				"src/it/readdsmf.c",
				"src/it/readmod.c",
				"src/it/readmod2.c",
				"src/it/readmtm.c",
				"src/it/readokt.c",
				"src/it/readokt2.c",
				"src/it/readoldpsm.c",
				"src/it/readpsm.c",
				"src/it/readptm.c",
				"src/it/readriff.c",
				"src/it/reads3m.c",
				"src/it/reads3m2.c",
				"src/it/readstm.c",
				"src/it/readstm2.c",
				"src/it/readxm.c",
				"src/it/readxm2.c",
				"src/it/xmeffect.c",
				"include/dumb.h",
				"include/internal/aldumb.h",
				"include/internal/barray.h",
				"include/internal/blip_buf.h",
				"include/internal/dumb.h",
				"include/internal/dumbfile.h",
				"include/internal/fir_resampler.h",
				"include/internal/it.h",
				"include/internal/lanczos_resampler.h",
				"include/internal/lpc.h",
				"include/internal/riff.h",
				"include/internal/stack_alloc.h",
				"include/internal/tarray.h",
			},
			Propagate = {
				Includes = { "dumb/dumb/include" },
			},
			Includes = {
				"dumb/dumb/include",
			},
			SourceDir = "dumb/dumb/",
		}
		local json = StaticLibrary {
			Name = "json",
			Sources = {
				"src/json/json-forwards.h",
				"src/json/json.h",
				"src/jsoncpp.cpp",
			}
		}
		local stb_image = StaticLibrary {
			Name = "stb_image",
			Sources = {
				"src/stb_image.c",
				"src/stb_image.h",
			}
		}
		local pixlib = StaticLibrary {
			Name = "pixlib",
			Sources = {
				"src/audio.cc",
				"src/audio.h",
				"src/bitmap_font.cc",
				"src/bitmap_font.h",
				"src/bmfont.cc",
				"src/bmfont.h",
				"src/dumb_source.cc",
				"src/dumb_source.h",
				"src/error_context.cc",
				"src/error_context.h",
				"src/opengl.h",
				"src/pix.cc",
				"src/pix.h",
				"src/sprite_sheet.cc",
				"src/sprite_sheet.h",
				"src/string_token.cc",
				"src/string_token.h",
				"src/vfs.cc",
				"src/vfs.h",
			},
			Propagate = {
				Libs = {
					{
						Config = "win32-*-*";
						"glfw3", "glew32",
						"opengl32", "glu32", "gdi32",
					},
					{
						Config = "linux-*-*";
						"glfw", "GLEW", "GLU", "GL",
						"X11", "Xi", "Xrandr", "Xxf86vm",
						"pthread",
					},
				},
			},
			Depends = { dumb, json, stb_image },
		}
		local aliens = Program {
			Name = "aliens",
			Sources = {
				"src/aliens_game.cc",
				"src/aliens_game.h",
			},
			Includes = {
				"src",
			},
			Libs = {
				{ "gorilla", "openal"; Config = "linux-*" },
				{ "gorilla", "OpenAL32"; Config = "win32-gcc-*" },
			},
			Depends = {
				pixlib,
				json, stb_image, dumb,
			},
		}
		local pixlets = Program {
			Name = "pixlets",
			Sources = {
				"src/main.cc",
			},
			Includes = {
				"src",
			},
			Depends = {
				pixlib,
			},
		}
		Default(aliens)
	end,

	Configs = {
		Config {
			Name = "linux-gcc",
			Tools = { "gcc" },
			ReplaceEnv = {
				CXX = "g++",
				LD = "$(CXX)",
			},
			Env = {
				CPPPATH = {
					"/home/zao/libs/gcc/include",
				},
				LIBPATH = {
					"/home/zao/libs/gcc/lib",
					"/home/zao/libs/gcc/lib64",
				},
				CCOPTS = {
					"-fPIC",
					{ "-O3"; Config = "*-*-production-*" },
				},
				CXXOPTS = {
					"-std=c++11",
					"-fPIC",
					{ "-g"; Config = "*-*-debug" },
					{ "-O3"; Config = "*-*-production-*" },
				},
				PROGOPTS  = { "-std=c++11", { "-g"; Config = "*-*-debug"}, "-Wl,-rpath=\\$ORIGIN" },
				SHLIBOPTS = { "-std=c++11", { "-g"; Config = "*-*-debug"}, "-Wl,-rpath=\\$ORIGIN" },
			},
			SupportedHosts = { "linux" },
			DefaultOnHost = "linux",
		},
		Config {
			Name = "win32-gcc",
			Tools = { "gcc" },
			ReplaceEnv = {
				CC = "gcc",
				CXX = "g++",
				LD = "$(CXX)",
			},
			Env = {
				CPPDEFS = { "GLEW_STATIC=1" },
				CPPPATH = {
					"C:/libs/gcc/include",
				},
				LIBPATH = {
					"C:/libs/gcc/lib",
				},
				CXXOPTS = {
					"-std=c++11",
					{ "-g"; Config = "*-*-debug" },
					{ "-O3"; Config = "*-*-production-*" },
				},
				PROGOPTS  = { "-std=c++11", { "-g"; Config = "*-*-debug"}  },
				SHLIBOPTS = { "-std=c++11", { "-g"; Config = "*-*-debug"}  },
			},
			SupportedHosts = { "windows" },
			DefaultOnHost = "windows",
		},
		Config {
			Name = "macosx-clang",
			Tools = { "clang-osx" },
			DefaultOnHost = "macosx",
		},
	},
}
